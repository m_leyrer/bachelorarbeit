function indices=permuteIndices(n)
    free = true(1,n);
    indices = zeros(1,n);
    ptr = 1;
    while any(free)
        i = round(rand()*(n-1)+1);
        if free(i)
           free(i) = false;
           indices(ptr) = i;
           ptr = ptr + 1;
        end
    end
    
end