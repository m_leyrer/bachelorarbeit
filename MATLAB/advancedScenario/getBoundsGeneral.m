% gets boundaries for each variable of a function s.t. the function is
% within its lower and upper boundaries
%
% version for arbitrary scenarios

function [a,b,n_tot] = getBoundsGeneral(objFun, varSize, sigma, scale, accuracy, lower, upper)
    global circle
    a = nan(1,varSize(2));
    n_tot = 0;
    lim = sigma\scale*20;
    err = circle;
    
    for i = 1:max(varSize)
        [x,sig,n_sim]=approach(objFun,@(r)r>upper,sigma,varSize,i,accuracy,err);
        n_tot = n_tot + n_sim;
        a(i) = sig*norm(sigma\x(:,i));
    end
    
    b = a;
    
    for i = find(isnan(b))
        [x,sig,n_sim]=approach(objFun,@(r)r>=lower,sigma,varSize,i,accuracy,err);
        n_tot = n_tot + n_sim;
        b(i) = sig*norm(sigma\x(:,i));
    end
    
end

function [x,sig,n_sim]=approach(objFun,outside,sigma,varSize,currIdx,accuracy,err)
    n = -1;
    n_sim = 0;
    x = nan(varSize);
    x(:,currIdx) = 0;
    out = false(1,length(err));
    res = objFun(x);
    sig = -1;
    C = sigma^2;
    
    % beta = @(x)sqrt(sum((sigma\x).^2,1));
    
    % scale error according to sigma
    err = sigma*err;
    
    if outside(res)
        sig = 1;
        outside=@(x)~outside(x);
    end
    
    % search a starting point
    while all(~out)
        n = n + 1;
        
        for i = 1:(length(err)-1)
           tmp = insAdd(x,err(:,i)*1.7^n,currIdx);
           out(i) = outside(objFun(tmp)) ;
           n_sim = n_sim + 1;
        end
        
        if n>40
            x(:,currIdx) = nan;
            return;
        end
    end
    
        
    % select the middle starting point
    outIdx = find(out);
    middleIdx = outIdx(round(length(outIdx)/2+.5));
    x = insAdd(x,err(:,middleIdx)*1.7^n,currIdx);

    % converge
    while n>accuracy
        n = n - 1;
        
        % calculate directional derivative of beta-norm for all directions
        betaDeriv = x(:,currIdx)'*(C\err);
        % sort by said derivative
        [~,indices] = sort(betaDeriv,'ascend');
        
        for k = 1:length(indices)
            i = indices(k);
            % break if not feasible (also includes all points afterwards)
            if betaDeriv(i)>=0
                break;
            end

            % check if point is ok (i.e., outside)
            tmp = insAdd(x,err(:,i)*1.7^n,currIdx);
            ok = outside(objFun(tmp));
            n_sim = n_sim + 1;

            % the first occurance of outside has the smallest beta-norm
            % and is thus the best choice
            if ok
                x = tmp;
                break;
            end
        end

    end
    
%     for k = 1:10
%         feasible = err(:,sqrt(sum((x(:,i)+err).^2,1))<norm(x(:,i)));
%         isOk = false(1,size(feasible,2));
%         for l = 1:size(feasible,2)
%             isOk(l) = ~outside(objFun(sigma*insAdd(x,feasible(:,l),i)));
%             n_sim = n_sim + 1;
%         end
%         feasible = feasible(:,isOk);
%         if(isempty(feasible)); break; end
%         distances = sqrt(sum((x(:,i)+feasible).^2,1));
%         x(:,i) = x(:,i)+feasible(:,distances==min(distances));
%     end


%             if n == accuracy
%                 tmp = x;
%                 tmp(:,i) = rotate(x(:,i),phi);
%                 if ~outside(objFun(sigma*tmp))
%                     x = tmp;
%                     scale = rotate(scale,phi);
%                     n = 0;
%                 end
%                 tmp(:,i) = rotate(x(:,i),-phi);
%                 if ~outside(objFun(sigma*tmp))
%                     x = tmp;
%                     scale = rotate(scale,-phi);
%                     n = 0;
%                 end
%                 
%             end
end

function x=insAdd(x,a,i)
    x(:,i)=x(:,i)+a(:,1);
end