% gets boundaries for each variable of a function s.t. the function is
% within its lower and upper boundaries
%
% specifically designed for the scenario at hand

function [a,b,n_tot] = getBoundsLin(objFun, varSize, sigma, scale, accuracy, lower, upper)
    x = zeros(varSize);
    a = nan(1,varSize(2));
    err = randn(varSize(1),1e3)*10;
    
    n_tot = 0;
    
    for i = 1:max(varSize)
        [a(i),n_sim]=approach(objFun,@(r)r>upper,sigma,varSize,i,err);
        n_tot = n_tot + n_sim;
        if isnan(a(i))
            break;
        end
    end
    
    b = a;
    
    for i = find(isnan(a))
        [b(i),n_sim]=approach(objFun,@(r)r>=lower,sigma,varSize,i,err);
        n_tot = n_tot + n_sim;
        if isnan(b(i))
            break;
        end
    end
    
end

function [a,n_sim]=approach(objFun,outside,sigma,varSize,i,err)
    n = 0;
    n_sim = 0;
    x = nan(varSize);
    x(:,i) = 0;
    res = objFun(x);
    sig = -1;
    out = false(1,length(err));
    b = zeros(varSize(1),1);
    
    beta_sq = @(x)x'*sigma*x;
    
    if outside(res)
        sig = 1;
        outside=@(x)~outside(x);
    end
    
    for j = 1:length(err)
        out(j) = outside(objFun(ins(x,err(j),i)));
    end
    
    if any(out)
        % Find separating hyperplane
        
        
        
        a = sig*a;
    else
        a = nan;
    end
    
    
    a = sig*sqrt(a);
    
    
end

% scales!!!
function y=rotate(x,phi)
    c = cos(phi);
    s = sin(phi);
    y = [c,-s;s,c]*x;
end

function x=insAdd(x,a,i)
    x(:,i)=x(:,i)+a;
end

function x=ins(x,a,i)
    x(:,i) = a(:,1);
end

function [c,ceq]=constrFun(outside, x)
    c = -outside(x);
    ceq = [];
end