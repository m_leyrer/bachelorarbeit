% gets boundaries for each variable of a function s.t. the function is
% within its lower and upper boundaries
%
% version for arbitrary scenarios

function [a,b,n_tot] = getBoundsGeneral2(objFun, varSize, sigma, scale, accuracy, lower, upper)
    global indices circle


    a = zeros(1,varSize(2));
    b = a;
    n_tot = 0;
    
    % err=randn([varSize(1),5e1]);
    err = circle;
    
    indices = permuteIndices(length(err));
    
    
    for i = 1:varSize(2)
        [d,n_sim]=sampleMin(objFun,@(r)r>upper,sigma,varSize,i,err,accuracy);
        n_tot = n_tot + n_sim;
        a(i) = d;
    end
    
    
    
    for i = 1:varSize(2)
        [d,n_sim]=sampleMin(objFun,@(r)r>=lower,sigma,varSize,i,err,accuracy);
        n_tot = n_tot + n_sim;
        b(i) = d;
    end
    
end


function [d,n_sim]=sampleMin(objFun,outside,sigma,varSize,i,err,accuracy)
    global indices
    
    n_sim = 0;
    n = 0;
    x = nan(varSize);
    x(:,i) = 0;
    res = objFun(x);
    sig = -1;
    d = nan;
    
    if outside(res)
        sig = 1;
        outside=@(x)~outside(x);
    end
    
    while n<30 && n>accuracy

        thresh = 0;
        isOk = false(1,length(err));
        feasible = x(:,i)'*err<=0;

        for k_pre = 1:length(err)
            k = indices(k_pre);
            if feasible(k) && x(:,i)'*err(:,k)<=thresh
                tmp = insAdd(x,err(:,k),i);
                res = objFun(tmp);
                isOk(k) = outside(res);
                if isOk(k)
                    thresh = x(:,i)'*err(:,k);
                end
                n_sim = n_sim + 1;
            end
        end
        
        if any(isOk) % proceed with next smaller step
            dists = sqrt(sum((sigma\(err+x(:,i))).^2,1));
            x = insAdd(x,err(:,dists==min(dists(isOk))),i);
            d = sig*norm(sigma\x(:,i));
            err = err/2;
            n = n - 1;
        else % grow larger
            err = err*2;
            n = n + 1;
            
        end
            
        
    end
end

function x=insAdd(x,a,i)
    k = round(rand()*(size(a,2)-1)+1);
    x(:,i)=x(:,i)+a(:,k);
end

function x=ins(x,a,i)
    x(:,i)=a(:,i);
end