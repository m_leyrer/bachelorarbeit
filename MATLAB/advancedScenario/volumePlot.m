% plot contour surfaces of a volume

function volumePlot(x,y,z,v,values)

    if ~exist('values','var'); values = [.2,.5,.7,.8,.9,.99]; end

    
    for i = 1:length(values)
       isosurface(x,y,z,v,values(i))
       hold on
    end
    
    hold off
    
    alpha .5
    
    xlim([x(1),x(length(x))])
    ylim([y(1),y(length(y))])
    zlim([z(1),z(length(z))])
    
    xlabel '\sigma_x'
    ylabel '\tau'
    zlabel '\sigma_v'
end

