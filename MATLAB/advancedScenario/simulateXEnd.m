% simulates the result for x_end for a given set of variables and 
% measurement errors in x and v

function x_end = simulateXEnd(x_0,v_0,a,tau,f_s,lim,err)
    
    x = x_0;
    v = v_0;
    n = 0;
    err=[err,nan(size(err,1),lim-size(err,2))];
    x_end=-1e3;
    while n<lim
        n = n+1;
        if -(x+err(1,n))/(v+err(2,n)) < tau
            x_end = x - v^2/(2*a);
            return;
        end
        x = x + v/f_s;
    end
    
end