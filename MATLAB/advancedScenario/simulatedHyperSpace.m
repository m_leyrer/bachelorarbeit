% Get a yield estimate by new method, integrating over a subspace with
% linear, orthogonal boundary hyperplanes
%
% Only works for independent random variables

simpleAEBClosedForm;

tic;

global f_s x_0 v_0 a x_min x_max sigma_x sigma_v tau

setGlobalVars();

varSize = [2,-ceil(f_s*x_0/v_0/3*2)+5];



A = zeros(varSize(2),length(tau),length(sigma_x),length(sigma_v));
B = A;


n_tot = 0;
n_sims = zeros([length(tau),length(sigma_x),length(sigma_v)]);

simFun = @(tau)@(err)simulateXEnd(x_0,v_0,a,tau,f_s,3*f_s,err);

yieldHS = zeros(length(tau),length(sigma_x),length(sigma_v));
boundsearchTime = yieldHS;


for t = 1:length(tau)
    for s = 1:length(sigma_x)
        for s2 = 1:length(sigma_v)

            fprintf("Step: %d:%d:%d\n", t, s, s2);
            scale = [1;tau(t)];
            sigma = [sigma_x(s),0;0,sigma_v(s2)];
            
            t_0 = toc;
            [A(:,t,s,s2),B(:,t,s,s2),n_sims(t,s,s2)] = getBoundsGeneral(simFun(tau(t)),varSize,sigma,scale,-20,x_min,x_max);
            boundsearchTime(t,s,s2) = toc - t_0;
            
            bad = A(~isnan(A(:,t,s,s2)),t,s,s2);
            good = B(isnan(A(:,t,s,s2)) & ~isnan(B(:,t,s,s2)),t,s,s2);
            n_tot = n_tot + n_sims(t,s,s2);
    
            yieldHS(t,s,s2) = prod(normcdf(-bad))*(1-prod(normcdf(-good)));
        end
    end
    

end


volumePlot(sigma_x,tau,sigma_v,yieldHS);
hold on;
volumePlot(sigma_x,tau,sigma_v,P);




executionTime = toc