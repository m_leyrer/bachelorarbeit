tic;

global f_s sigma_x sigma_v tau mcRuns

setGlobalVars();

mcRuns = 1e4;

err = randn(2,ceil(f_s),mcRuns);

yieldMC = zeros([length(tau),length(sigma_x),length(sigma_v)]);

for i = 1:length(tau)
    for j = 1:length(sigma_x)
        for k = 1:length(sigma_v)
            yieldMC(i,j,k) = monteCarloSim(err,tau(i),sigma_x(j),sigma_v(k));
            fprintf("Step %d:%d:%d\n", i, j, k);
        end
        toc
    end
end


monteCarloTime = toc;

% % Non-linear constraint function for optimization
% constr = @(p_min)@(x)ndgrid(p_min-yieldMC(round(x(2)),round(x(1))),0);
% 
% % Find optimum
% [~,max_y] = max(yieldMC);
% [~,max_x] = max(max(yieldMC));
% x = patternsearch(@(x)-x(1),[max_x,max_y(max_x)+2],[],[],[],[],[1,1],[length(tau),length(sigma)],constr(.99));
% sigma_max = sigma(round(x(1)));
% tau_opt = tau(round(x(2)));
% 
% % Plot results
% figure(1);
% surf(sigma,tau,yieldMC);
% xlabel("sigma in m");
% ylabel("tau in s");
% 
% % figure(2);
% % contour(sigma,tau,P,[.99,.9,.8,.7,.6,.5,.4,.3])
% % hold on
% % plot(tau_opt,sigma_max);



function p = monteCarloSim(err, tau, sigma_x, sigma_v)
    global f_s x_0 v_0 a x_min x_max mcRuns
    
    n = 1;
    n_f = 0;
    n_s = 0;
    
    sigma = [sigma_x , 0;
             0 , sigma_v];
    
    while (n_f < 100 || n_s < 100) && n <= mcRuns
        x = simulateXEnd(x_0,v_0,a,tau,f_s,3*f_s,sigma*err(:,:,n));
        if x >= x_min && x <= x_max
            n_s = n_s+1;
        else
            n_f = n_f+1;
        end
        n = n+1;
    end
    p = n_s/(n_f + n_s);
end

