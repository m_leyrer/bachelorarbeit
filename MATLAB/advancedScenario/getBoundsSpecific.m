% gets boundaries for each variable of a function s.t. the function is
% within its lower and upper boundaries
%
% specifically designed for the scenario at hand

function [a,b,n_tot] = getBoundsSpecific(objFun, varSize, sigma, scale, accuracy, lower, upper)
    x = zeros(varSize);
    a = nan(1,varSize(2));
    
    n_tot = 0;
    
    for i = 1:max(varSize)
        [x,sig,n_sim]=approach(objFun,@(r)r>upper,scale,sigma,varSize,i,accuracy);
        n_tot = n_tot + n_sim;
        a(i) = sig*norm(x(:,i));
        if isnan(a(i))
            break;
        end
    end
    
    b = a;
    
    for i = find(isnan(a))
        [x,sig,n_sim]=approach(objFun,@(r)r>=lower,scale,sigma,varSize,i,accuracy);
        n_tot = n_tot + n_sim;
        b(i) = sig*norm(x(:,i));
        if isnan(b(i))
            break;
        end
    end
    
end

function [x,sig,n_sim]=approach(objFun,outside,scale,sigma,varSize,i,accuracy)
    n = 0;
    n_sim = 0;
    x = nan(varSize);
    x(:,i) = 0;
    res = objFun(sigma*x);
    dir = 1;
    sig = -1;
    
    if outside(res)
        sig = 1;
        outside=@(x)~outside(x);
    end
    
    while n>accuracy
        res = objFun(sigma*x);
        if ~outside(res)
            x(:,i) = x(:,i)+sig*scale.*2^n;
        else
            dir = -1;
            x(:,i) = x(:,i)-sig*scale.*2^n;
        end
        n_sim = n_sim + 1;
        n = n + dir;
        if n>30
            x(:,i) = nan;
            break;
        end
    end
    
    
    
end

% scales!!!
function y=rotate(x,phi)
    c = cos(phi);
    s = sin(phi);
    y = [c,-s;s,c]*x;
end

function x=insAdd(x,a,i)
    x(:,i)=x(:,i)+a;
end