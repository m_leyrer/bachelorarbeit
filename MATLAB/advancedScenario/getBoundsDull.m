% gets boundaries for each variable of a function s.t. the function is
% within its lower and upper boundaries
%
% version for arbitrary scenarios

function [a,b,n_tot] = getBoundsDull(objFun, varSize, sigma, scale, accuracy, lower, upper)
    x = nan(varSize);
    a = zeros(1,varSize(2));
    b = a;
    n_tot = 0;
    err=randn([varSize(1),1e4]);
    
    for i = 1:varSize(2)
        [dist,n_sim]=sampleMin(objFun,@(r)r>upper,scale,sigma,varSize,i,err);
        n_tot = n_tot + n_sim;
        a(i) = dist;
    end
    
    
    
    for i = 1:varSize(2)
        [dist,n_sim]=sampleMin(objFun,@(r)r>=lower,scale,sigma,varSize,i,err);
        n_tot = n_tot + n_sim;
        b(i) = dist;
    end
    
end


function [dist,n_sim]=sampleMin(objFun,outside,scale,sigma,varSize,i,err)
    n_sim = 0;
    x = nan(varSize);
    x(:,i) = 0;
    res = objFun(x);
    isOk = false(1,length(err));
    sig = -1;
    
    if outside(res)
        sig = 1;
        outside=@(x)~outside(x);
    end
    
    for k = 1:length(err)
        isOk(k) = outside(objFun(ins(x,err(:,k),i)));
        n_sim = n_sim + 1;
    end
    
    dist = sig*min(sqrt(sum((sigma\err(:,isOk)).^2,1)));
    if isempty(dist); dist = nan; end
end

function x=insAdd(x,a,i)
    x(:,i)=x(:,i)+a;
end

function x=ins(x,a,i)
    x(:,i)=a;
end