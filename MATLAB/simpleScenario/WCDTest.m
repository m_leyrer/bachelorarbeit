% Testing environment for WCD functions

global f_s x_0 v_0 a x_min x_max npp

f_s = 1e1;  % 1/s
x_0 = 10;   % m
v_0 = -10;   % m/s
a = 10;     % m/s^2
npp = [0,0];

x_min = 2;
x_max = 2.501;
sigma = linspace(1e-9,1,50);
tau = linspace(.2,.8,50);
beta1 = ones(50,2) * -1e3;
beta2 = ones(50,2) * -1e3;


tries = 10;
n_sim = 1e6;
err = randn(n_sim,5)*3;

simFun = @(tau)@(err)simulateSimpleXEnd(x_0,v_0,a,tau,f_s,3e1,err);
% simFun = @(t)@(x)[1,-1,5,3,-2]*x'/6.3246+t*5;

for t = 1:length(tau)
   beta1(t,2) = getWCDPs(simFun(tau(t)),x_max,false,[1,5],tries);
   beta1(t,1) = getWCDPs(simFun(tau(t)),x_min,true,[1,5],tries);
   
   beta2(t,2) = getWCDRandGrid(simFun(tau(t)),x_max,false,err);
   beta2(t,1) = getWCDRandGrid(simFun(tau(t)),x_min,true,err);
end

yield1 = abs(normcdf(beta1(:,1)./sigma)-normcdf(-beta1(:,2)./sigma));
yield2 = abs(normcdf(beta2(:,1)./sigma)-normcdf(-beta2(:,2)./sigma));

figure(1); subplot(1,2,1); plot(beta1*[-1,0;0,1]); subplot(1,2,2); surf(yield1);
figure(2); subplot(1,2,1); plot(beta2*[-1,0;0,1]); subplot(1,2,2); surf(yield2);