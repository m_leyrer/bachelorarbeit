
% actually this is worse...
function beta = betterGetWCDRand(perfFun,lower,upper,errs,cycles)

    n_sim = size(errs,1);
    
    over = false(1,n_sim);
    under = false(1,n_sim);
    err = errs;
    
    for n = 1:cycles
        
        over(:) = false;
        under(:) = false;
        
        for i=1:n_sim
            res = perfFun(err(i,:));
            if res<lower
                under(i) = true;
            end
            if res>upper
                over(i) = true;
            end
        end

        res = perfFun(zeros(1,size(err,2)));
        
        
        if res<lower
            [beta_u,i_u] = min(sum(err(under==false,:).^2,2));
            beta_u = [-sqrt(beta_u),1e6];
        else
            [beta_u,i_u] = min(sum(err(under==true,:).^2,2));
            beta_u = [sqrt(beta_u),1e6];
        end

        if res>upper
            [beta_o,i_o] = min(sum(err(over==false,:).^2,2));
            beta_o = [-sqrt(beta_o),1e6];
        else
            [beta_o,i_o] = min(sum(err(over==true,:).^2,2));
            beta_o = [sqrt(beta_o),1e6];
        end
        
        if i_u>0
            err(1:floor(size(err,1)/2),:) = repmat(err(i_u,:),[floor(size(err,1)/2),1]) + errs(1:floor(size(err,1)/2),:);
        end
        if i_o>0
            err(floor(size(err,1)/2)+1:size(err,1),:) = repmat(err(i_o,:),[ceil(size(err,1)/2),1]) + errs(floor(size(err,1)/2)+1:size(err,1),:);
        end
    end
    
    beta = [beta_u(1);beta_o(1)];
end