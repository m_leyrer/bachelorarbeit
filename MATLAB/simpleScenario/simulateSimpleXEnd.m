

function x_end = simulateSimpleXEnd(x_0,v_0,a_b,tau,f_s,lim,err)
    
    x = x_0;
    v = v_0;
    n = 0;
    br = 0;
    err=[err,zeros(1,lim-length(err))];
    while v < 0
        n = n+1;
        if -(x+err(n))/v < tau
            br = 1;
        end
        x = x + v/f_s;
        if br == 1
            v = v + a_b/f_s;
        end
        if n > lim
           fprintf("No collision detected.\n");
           return;
        end 
    end
    
    x_end = x;
end