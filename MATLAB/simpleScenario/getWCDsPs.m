function beta = getWCDsPs(perfFun,lower,upper,varSize,runs)
    global npp
    
    beta = [1e6,1e6];
    sgn = [1,1];
    
    dist = @(x)sum(x.^2);
    
    ref = perfFun(zeros(varSize));
    
    
    if ref < lower
        constr_l = @(err)ndgrid(lower-perfFun(err),0);
        sgn(1) = -1;
    else
        constr_l = @(err)ndgrid(perfFun(err)-lower,0);
    end
    
    for i = 1:runs
        start = getStartingPoint(perfFun,constr_l,varSize,4);
        [~,beta_l] = patternsearch(dist,start,[],[],[],[],[],[],constr_l);
        if beta_l < beta(1)
            npp = npp + [1,0];
            beta(1) = beta_l;
        end
    end
    
    
    if ref > upper
        constr_u = @(err)ndgrid(perfFun(err)-upper,0);
        sgn(2) = -1;
    else
        constr_u = @(err)ndgrid(upper-perfFun(err),0);
    end
    
    for i = 1:runs
        start = getStartingPoint(perfFun,constr_u,varSize,4);
        [~,beta_u] = patternsearch(dist,start,[],[],[],[],[],[],constr_u);
        if beta_u < beta(2)
            npp = npp + [0,1];
            beta(2) = beta_u;
        end
    end
    
    beta(beta==1e6) = -1e6;
    
    beta = sgn.*sqrt(beta);
    
end