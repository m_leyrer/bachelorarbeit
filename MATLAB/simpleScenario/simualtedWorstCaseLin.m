global f_s x_0 v_0 a x_min x_max

f_s = 1e1;  % 1/s
x_0 = 10;   % m
v_0 = -10;   % m/s
a = 10;     % m/s^2

x_min = 0;
x_max = .5;
sigma = linspace(1e-9,1,50);
tau = linspace(0.45,0.85,50);

n_sim = 1e7;
err = randn(n_sim,2e1);
over = false(1,1e3);
under = false(1,1e3);
xEnd = 0;

simFun = @(tau)@(err)simulateSimpleXEnd(x_0,v_0,0,a,tau,f_s,2e1,err);


yield = zeros(length(tau),length(sigma));
yield_glb = yield;
yield_lub = yield;

under(:) = false;
over(:) = false;

beta = getWCD(simFun(tau(25)),x_min,x_max,err);
beta2 = getWCD(simFun(tau(35)),x_min,x_max,err);

delta = [0,0];
delta(1) = (beta2(1)-beta(1))/10;
delta(2) = (beta2(2)-beta(2))/10;

for t = 1:length(tau)
    for s = 1:length(sigma)
        yield_lub(t,s) = min(normcdf((beta(1)+delta(1)*(t-25))/sigma(s)),normcdf((beta(2)+delta(2)*(t-25))/sigma(s)));
        yield(t,s) = abs(normcdf((beta(1)+delta(1)*(t-25))/sigma(s))-normcdf(-(beta(2)+delta(2)*(t-25))/sigma(s)));
        yield_glb(t,s) = normcdf((beta(1)+delta(1)*(t-25))/sigma(s))+normcdf((beta(2)+delta(2)*(t-25))/sigma(s))-2;
    end
end

% Non-linear constraint function for optimization
constr = @(p_min)@(x)ndgrid(p_min-yield(round(x(2)),round(x(1))),0);

% Find optimum
[~,max_y] = max(yield);
[~,max_x] = max(max(yield));
x = patternsearch(@(x)-x(1),[max_x,max_y(max_x)+2],[],[],[],[],[1,1],[length(tau),length(sigma)],constr(.99));
sigma_max = sigma(round(x(1)));
tau_opt = tau(round(x(2)));

figure(1);
contour(sigma,tau,yield,[.99,.9,.8,.7,.6,.5,.4,.3]);
hold on
plot(tau_opt,sigma_max,"r*");

function beta = getWCD(perfFun,lower,upper,err)

    n_sim = length(err);
    
    over = false(1,n_sim);
    under = false(1,n_sim);
    
    for i=1:n_sim
        res = perfFun(err(i,:));
        if res<lower
            under(i) = true;
        end
        if res>upper
            over(i) = true;
        end
    end

    res = perfFun(zeros(1,size(err,2)));

    if res<lower
        beta_u = -sqrt([min(sum(err(under==false,:).^2,2)),1e6]);

    else
        beta_u = sqrt([min(sum(err(under==true,:).^2,2)),1e6]);
    end

    if res>upper
        beta_o = -sqrt([min(sum(err(over==false,:).^2,2)),1e6]);
    else
        beta_o = sqrt([min(sum(err(over==true,:).^2,2)),1e6]);
    end
    
    beta = [beta_u;beta_o];
end