% Gives the linearization of a constraint function as a*x = alpha
% i.e. as a hyperplane

function [a,alpha] = getLinearConstraint(constrFun,varSize,scale,points,runs)
    x = randn([runs,max(varSize)]) * scale;
    b = zeros([runs,1]);
    
    for i = 1:runs
       b(i) = constrFun(x(i,:));
    end
    
    n = 0;
    del = 1e-9*scale;
    while n<points
        del = del * 10;
        n = length(b(abs(b)<del));
    end
    
    X = x(abs(b)<del,:);
    
    alpha = -constrFun(zeros(varSize));
    al = repmat(alpha,[n,1]);
    
    a = X \ al;
    
    alpha = alpha/norm(a);
    a = a/norm(a);
    
end