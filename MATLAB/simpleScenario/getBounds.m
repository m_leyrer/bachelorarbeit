% gets boundaries for each variable of a function s.t. the function is
% within its lower and upper boundaries

function [a,b,n_sim] = getBounds(objFun, varSize, scale, lower, upper)
    x = zeros(varSize);
    a = x;
    b = x;
    n_sim = 0;
    lim = scale*10;
    % determine a
    
    for i = 1:max(varSize)
        n = 0;
        x = ones(varSize)*1000*scale;
        x(i) = 0;
        res = objFun(x);
        if res>upper
            while res>=upper
                x(i) = x(i)+.01*scale;
                res = objFun(x);
                n_sim = n_sim + 1;
                if abs(x(i))>lim
                    x(i) = nan;
                    break;
                end
            end
        else
            while res<upper
                x(i) = x(i)-.01*scale;
                res = objFun(x);
                n_sim = n_sim + 1;
                if abs(x(i))>lim
                    x(i) = nan;
                    break;
                end
            end
        end
        
        a(i) = x(i);
    end
    
    for i = 1:max(varSize)
        n = 0;
        x = ones(varSize)*1000*scale;
        x(i) = 0;
        res = objFun(x);
        if res<lower
            while res<=lower
                x(i) = x(i)-.01*scale;
                res = objFun(x);
                n_sim = n_sim + 1;
                if abs(x(i))>lim
                    x(i) = nan;
                    break;
                end
            end
        else
            while res>lower
                x(i) = x(i)+.01*scale;
                res = objFun(x);
                n_sim = n_sim + 1;
                if abs(x(i))>lim
                    x(i) = nan;
                    break;
                end
            end
        end
        
        b(i) = x(i);
    end
    
end