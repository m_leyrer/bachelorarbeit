function beta = getWCDPs(perfFun,limit,neg,varSize,runs)

    beta = 1e6;
    sgn = 1;
    
    dist = @(x)sum(x.^2);
    
    ref = perfFun(zeros(varSize));    
    
    if ~neg
        if ref > limit
            constr = @(err)compare(perfFun(err),limit);
            sgn = -1;
            start = -1e3*ones(varSize);
        else
            constr = @(err)compare(limit,perfFun(err));
            start = 1e3*ones(varSize);
        end
    else
        if ref < limit
            constr = @(err)compare(limit,perfFun(err));
            start = 1e3*ones(varSize);
            sgn = -1;
        else
            constr = @(err)compare(perfFun(err),limit);
            start = -1e3*ones(varSize);
        end
    end
    
    options = optimoptions('patternsearch');%,'MaxFunctionEvaluations',1000);
    
    for i = 1:runs

        [~,tmp] = patternsearch(dist,start+randn(varSize)*100,[],[],[],[],[],[],constr,options);
        beta = min(tmp,beta);
    end
    
    beta(beta==1e6) = nan;
    
    beta = sgn*sqrt(beta);
    
end

function [comp,c] = compare(a,b)
    comp = a - b;
    c = [];
end