% gets boundaries for each variable of a function s.t. the function is
% within its lower and upper boundaries
%
% faster version

function [a,b,n_sim] = getBoundsOpt(objFun, varSize, scale, accuracy, lower, upper)
    x = zeros(varSize);
    a = x;
    b = x;
    n_sim = 0;
    lim = scale*100;
    % determine a
    
    for i = 1:max(varSize)
        n = 0;
        x = ones(varSize)*1000*scale;
        x(i) = 0;
        res = objFun(x);
        dir = 1;
        sig = 1;
        if res>upper
            while n>accuracy
                res = objFun(x);
                if res > upper
                    x(i) = x(i)+scale*2^n;
                else
                    dir = -1;
                    x(i) = x(i)-scale*2^n;
                end
                n_sim = n_sim + 1;
                n = n + dir;
                if abs(x(i))>lim
                    x(i) = nan;
                    break;
                end
            end
        else
            sig = -1;
            while n>accuracy
                res = objFun(x);
                if res <= upper
                    x(i) = x(i)-scale*2^n;
                else
                    dir = -1;
                    x(i) = x(i)+scale*2^n;
                end
                n_sim = n_sim + 1;
                n = n + dir;
                if abs(x(i))>lim
                    x(i) = nan;
                    break;
                end
            end
        end
        
        a(i) = sig*abs(x(i));
    end
    
    for i = 1:max(varSize)
        n = 0;
        x = ones(varSize)*1000*scale;
        x(i) = 0;
        res = objFun(x);
        dir = 1;
        sig = 1;
        if res<lower
            sig = -1;
            while n>accuracy
                res = objFun(x);
                if res < lower
                    x(i) = x(i)-scale*2^n;
                else
                    dir = -1;
                    x(i) = x(i)+scale*2^n;
                end
                n_sim = n_sim + 1;
                n = n + dir;
                if abs(x(i))>lim
                    x(i) = nan;
                    break;
                end
            end
        else
            while n>accuracy
                res = objFun(x);
                if res >= lower
                    x(i) = x(i)+scale*2^n;
                else
                    dir = -1;
                    x(i) = x(i)-scale*2^n;
                end
                n_sim = n_sim + 1;
                n = n + dir;
                if abs(x(i))>lim
                    x(i) = nan;
                    break;
                end
            end
        end
        
        b(i) = sig*abs(x(i));
    end
    
end