function beta = getWCDRandGrid(perfFun,limit,neg,err)

    n_sim = length(err);
    
    out = false(1,n_sim);
    if ~neg
        outside = @(x)x>limit;
    else
        outside = @(x)x<limit;
    end
    
    for i=1:n_sim
        res = perfFun(err(i,:));
        if outside(res)
            out(i) = true;
        end
    end

    res = perfFun(zeros(1,size(err,2)));

    if outside(res)
        beta = -sqrt(min(sum(err(out==false,:).^2,2)));
        if isempty(beta)
            beta = -1e3;
        end
    else
        beta = sqrt(min(sum(err(out==true,:).^2,2)));
        if isempty(beta)
            beta = 1e3;
        end
    end
    
    
    
end
