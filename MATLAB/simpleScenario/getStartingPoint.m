% This function determines a point x where the objective function
% does NOT fulfil the constraint (i.e. constr(res) >= 0)

function [x,n] = getStartingPoint(constr,varSize)

    a = 1;
    n = 0;
    
    while a >= 0
        x = randn(varSize)*n;
        a = constr(x);
        n = n + 1;
    end
    
end

