% Get a yield estimate by finding the WCD

tic;

global f_s x_0 v_0 a x_min x_max sigma_x tau

setGlobalVars();
simpleAEBClosedForm;

varSize = [1,-ceil(f_s*x_0/v_0/3*2)];

sigma = sigma_x;
beta1 = nan(length(tau),2);
beta2 = nan(length(tau),2);



tries = 10;
n_sim = 4e5;
err = randn(n_sim,varSize(2))*3;
% over = false(1,n_sim);
% under = false(1,n_sim);
xEnd = 0;

simFun = @(tau)@(err)simulateXEnd(x_0,v_0,a,tau,f_s,3*f_s,err);

for t = 1:length(tau)
    
%     beta1(t,1) = getWCDPs(simFun(tau(t)),x_min,true,varSize,tries);
%     beta1(t,2) = getWCDPs(simFun(tau(t)),x_max,false,varSize,tries);
    beta2(t,1) = getWCDRandGrid(simFun(tau(t)),x_min,true,err);
    beta2(t,2) = getWCDRandGrid(simFun(tau(t)),x_max,false,err);

    fprintf("t: %d\n", t);

end

% beta1 = beta1(~any(isnan(beta1)),:);
% beta2 = beta2(~any(isnan(beta2)),:);

% yield1 = abs(normcdf(beta1(:,1)./sigma)-normcdf(-beta1(:,2)./sigma));
yield2 = abs(normcdf(beta2(:,1)./sigma)-normcdf(-beta2(:,2)./sigma));

% betaLin1 = linProj*beta1;
betaLin2 = linProj*beta2;
% yieldLin1 = abs(normcdf(betaLin1(:,1)./sigma)-normcdf(-betaLin1(:,2)./sigma));
yieldLin2 = abs(normcdf(betaLin2(:,1)./sigma)-normcdf(-betaLin2(:,2)./sigma));



% % Non-linear constraint function for optimization
% constr = @(p_min)@(x)ndgrid(p_min-yield(round(x(2)),round(x(1))),0);
% 
% % Find optimum
% [~,max_y] = max(yield);
% [~,max_x] = max(max(yield));
% x = patternsearch(@(x)-x(1),[max_x,max_y(max_x)+2],[],[],[],[],[1,1],[length(tau),length(sigma)],constr(.99));
% sigma_max = sigma(round(x(1)));
% tau_opt = tau(round(x(2)));

% figure(1);
% contour(sigma,tau,yield,[.99,.9,.8,.7,.6,.5,.4,.3]);
% hold on
% plot(sigma_max,tau_opt,"r*");

% figure(2);
% subplot(2,2,1); plot(beta1);
% subplot(2,2,3); plot(betaLin1);
% subplot(2,2,2); surf(yield1);
% subplot(2,2,4); surf(yieldLin1);

executionTime = toc


