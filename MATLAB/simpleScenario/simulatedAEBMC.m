tic;

global f_s x_0 v_0 a x_min x_max

f_s = 5e1;  % 1/s
x_0 = 10;   % m
v_0 = -10;   % m/s
a = 10;     % m/s^2

x_min = 0;
x_max = .5001;
sigma = linspace(1e-9,1,50);
tau = linspace(0.25,0.65,50);

err = randn(1e5,4e1);

yieldMC = zeros(50);

for i = 1:length(tau)
    for j = 1:length(sigma)
        yieldMC(i,j) = monteCarloSim(err,sigma(j),tau(i));
        fprintf("Step %d:%d\n", i, j);
    end
end

% Non-linear constraint function for optimization
constr = @(p_min)@(x)ndgrid(p_min-yieldMC(round(x(2)),round(x(1))),0);

% Find optimum
[~,max_y] = max(yieldMC);
[~,max_x] = max(max(yieldMC));
x = patternsearch(@(x)-x(1),[max_x,max_y(max_x)+2],[],[],[],[],[1,1],[length(tau),length(sigma)],constr(.99));
sigma_max = sigma(round(x(1)));
tau_opt = tau(round(x(2)));

% Plot results
figure(1);
surf(sigma,tau,yieldMC);
xlabel("sigma in m");
ylabel("tau in s");

% figure(2);
% contour(sigma,tau,P,[.99,.9,.8,.7,.6,.5,.4,.3])
% hold on
% plot(tau_opt,sigma_max);



function p = monteCarloSim(err, sigma, tau)
    global f_s x_0 v_0 a x_min x_max 
    
    n = 1;
    n_f = 0;
    n_s = 0;
    while (n_f < 500 || n_s < 500) && n <= 1e5
        x = simulateXEnd(x_0,v_0,a,tau,f_s,1e4,sigma*err(n,:));
        if x >= x_min && x <= x_max
            n_s = n_s+1;
        else
            n_f = n_f+1;
        end
        n = n+1;
    end
    p = n_s/(n_f + n_s);
end

