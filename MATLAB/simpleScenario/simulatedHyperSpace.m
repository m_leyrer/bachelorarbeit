% Get a yield estimate by new method, integrating over a subspace with
% linear, orthogonal boundary hyperplanes
%
% Only works for independent random variables

simpleAEBClosedForm;

tic;

global f_s x_0 v_0 a x_min x_max tau sigma_x

setGlobalVars();

varSize = [1,-ceil(f_s*x_0/v_0/3*2)];
scale = 1;

sigma = sigma_x;
A = zeros(max(varSize),length(tau));
B = A;
b1 = zeros(1,length(tau));
b2 = b1;
n_sims = zeros(1,length(tau));

linProj = [ones(size(tau));1:length(tau)]'/[ones(size(tau));1:length(tau)]';

n_tot = 0;

simFun = @(tau)@(err)simulateXEnd(x_0,v_0,a,tau,f_s,3*f_s,err);

yieldHS = zeros(length(tau),length(sigma));
% yieldWCA = yieldHS;
% yieldWCALin = yieldHS;

% TODO: get rid of tau offset!!!

for t = 1:length(tau)

    fprintf("Step: %d\n", t);

    [A(:,t),B(:,t),n_sims(t)] = getBoundsOpt(simFun(tau(t)),varSize,scale,-40,x_min,x_max);



    bad = A(~isnan(A(:,t)),t);
    good = B(isnan(A(:,t)) & ~isnan(B(:,t)),t);
    n_tot = n_tot + n_sims(t);
%     if any(bad>0)
%         b1(t) = -sqrt(sum(bad(bad>0).^2));
%     else  
%         b1(t) = min(abs(bad));
%     end
%     if all(good<0)
%         b2(t) = -min(abs(B(:,t)));
%     else
%         b2(t) = sqrt(sum(B(B(:,t)>0,t).^2));
%     end
    
    
    for s = 1:length(sigma)
        yieldHS(t,s) = prod(normcdf(-bad./sigma(s)))*(1-prod(normcdf(-good./sigma(s))));
%         yieldWCA(t,s) = abs(normcdf(b1(t)/sigma(s))*normcdf(b2(t)/sigma(s)));
    end
    

end
% 
% b1Lin = (linProj*b1')';
% b2Lin = (linProj*b2')';
% 
% yieldWCALin = abs(normcdf(b1Lin'./sigma).*normcdf(b2Lin'./sigma));

executionTime = toc

figure(1);
surf(sigma,tau,yieldHS,'FaceAlpha',.8);
hold on;
mesh(sigma,tau,P+.3,'FaceAlpha',.2);
xlabel("sigma in m");
ylabel("tau in s");
zlabel("P(x\in [x_{min},x_{max}])");

figure(2);
surf(sigma,tau,yieldHS);
xlabel("sigma in m");
ylabel("tau in s");
zlabel("\hat{P}(x_{end}\in [x_{min},x_{max}])");

figure(3);
surf(sigma,tau,P);
xlabel("sigma in m");
ylabel("tau in s");
zlabel("P(x_{end}\in [x_{min},x_{max}])");

figure(4);
surf(sigma,tau,yieldHS-P);
xlabel("sigma in m");
ylabel("tau in s");
zlabel("\hat{P}-P");
