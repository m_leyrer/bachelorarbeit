

function t_ttc = simulateSimpleTTC(x_0,v_0,a,f_s,lim)
    if x_0/v_0 > 0
        printf("No imminent collision.\n");
        return;
    end
    
    x = x_0;
    v = v_0;
    n = 0;
    l_a = length(a);
    while x > 0
        n = n+1;
        x = x + v/f_s;
        if n <= l_a
            v = v + a(n)/f_s;
        end
        if n > lim
           printf("No collision detected.\n");
           return;
        end 
    end
    
    t_ttc = n/f_s;
end