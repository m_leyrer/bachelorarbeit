function setGlobalVars()

    global f_s x_0 v_0 a x_min x_max sigma_x sigma_v tau

    f_s = 1e2;  % 1/s
    x_0 = 10;   % m
    v_0 = -10;   % m/s
    a = 10;     % m/s^2

    x_min = 0;
    x_max = .5001;
    sigma_x = linspace(1e-9,1,50);
    sigma_v = linspace(1e-9,1,50);
    tau = linspace(0.35,0.65,50);
end