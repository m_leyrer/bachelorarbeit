function beta = getWCDsRandGrid(perfFun,lower,upper,err)

    n_sim = length(err);
    
    over = false(1,n_sim);
    under = false(1,n_sim);
    
    for i=1:n_sim
        res = perfFun(err(i,:));
        if res<lower
            under(i) = true;
        end
        if res>upper
            over(i) = true;
        end
    end

    res = perfFun(zeros(1,size(err,2)));

    if res<lower
        beta_u = -sqrt(min(sum(err(under==false,:).^2,2)));
    else
        beta_u = sqrt(min(sum(err(under==true,:).^2,2)));
    end

    if res>upper
        beta_o = -sqrt(min(sum(err(over==false,:).^2,2)));
    else
        beta_o = sqrt(min(sum(err(over==true,:).^2,2)));
    end
    
    if size(beta_o) == [0,0]
        beta_o = -1e3;
    end
    
    if size(beta_u) == [0,0]
        beta_u = -1e3;
    end
    
    beta = [beta_u,beta_o];
end
