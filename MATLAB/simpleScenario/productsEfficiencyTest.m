% The purpose of this program is to determine the fastest way of
% realizing products over a large number of quantities.

t_1 = [];
t_2 = [];
t_3 = [];

for n = 1:1e2
    % The test vector whose elements are to be multiplied
    as = rand(1e6,1)+.5;

    % Method 1: a for loop
    tic;
    res_1 = 1;
    for i=1:length(as)
        res_1 = res_1*as(i);
    end
    t_1 = [t_1,toc];

    % Method 2: exp of sum log
    tic;
    res_2 = exp(sum(log(as)));
    t_2 = [t_2,toc];
    
    % Method 3: precompiled product
    tic;
    res_3 = prod(as);
    t_3 = [t_3,toc];
end
