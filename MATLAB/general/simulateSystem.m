% simulates the result for x(i), v(i), good(i) and bad(i)
% for a given set of variables

function [x,v,good,bad] = simulateSystem(x_0,v_0,a,f_s,lim)
    global x_min x_max

    x = nan(1,lim);
    v = x;
    bad = false(1,lim);
    good = bad;
    
    x(1) = x_0;
    v(1) = v_0;
    
    for n = 1:lim
        x_end = x(n) - v(n).^2/(2*a);
        bad(n) = x_end > x_max;
        good(n) = (x_end <= x_max) && (x_end >= x_min);
        if n<lim    
            x(n+1) = x(n) + v(n)/f_s;
            v(n+1) = v(n);
        end
    end
    
end