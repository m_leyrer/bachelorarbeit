% Get a yield estimate by new method, integrating over a subspace with
% linear, orthogonal boundary hyperplanes
%
% Only works for independent random variables



global f_s x_0 v_0 a x_min x_max sigma_x sigma_v tau

setGlobalVars();

varSize = [2,-ceil(f_s*x_0/v_0/3*2)+3];

ttcTriggerRule = @(x,v,tau)x+v*tau;
ttcTriggerRuleGrad = @(x,v,tau)[1;tau];
ttcTrigger = @(x,v,tau)ttcTriggerRule(x,v,tau)<=0;

btnTriggerRule = @(x,v,tau)x-v.^2/(2*tau);
btnTriggerRuleGrad = @(x,v,tau)[1;-v/tau];
btnTrigger = @(x,v,tau)btnTriggerRule(x,v,tau)<=0;

[x,v,good,bad] = simulateSystem(x_0,v_0,a,f_s,varSize(2));

wcd = zeros(varSize(2),length(tau),length(sigma_x),length(sigma_v));

yieldOWCA = zeros(length(tau),length(sigma_x),length(sigma_v));

n_sims = zeros(size(yieldOWCA));


tic;

for t = 1:length(tau)
    for s = 1:length(sigma_x)
        for s2 = 1:length(sigma_v)

            fprintf("Step: %d:%d:%d\n", t, s, s2);
            C = [sigma_x(s),0;0,sigma_v(s2)].^2;
            
            beta_sq = @(x)((x)'/C)*x;
            
            ttcTriggerMat = [1,tau(t)]';
            
            covInv = inv(C)+inv(C');
            
            constr = @(x)constrFun(x,@(x)ttcTriggerRule(x(1),x(2),tau(t)),@(x)ttcTriggerRuleGrad(x(1),x(2),tau(t)));
            
            for i = 1:varSize(2)
%                 % solve lagrangian for ttc
                r = [x(i);v(i)];
%                 alpha = -ttcTriggerMat'*r / ((ttcTriggerMat'/covInv)*ttcTriggerMat);
%                 r_opt = r + alpha * (covInv \ ttcTriggerMat);
                
                % solve lagrangian for btn ... or not because its fucking
% %                 hard!!! or just try it:
%                 
%                 r_optOfAlpha = @(a)[.5*sigma_x(s).^2,0;0,tau(t)*sigma_v(s2)^2/(2*tau(t)+a*sigma_v(s2)^2)]*[1;-r(2)/tau(t)]*a;
%                 
%                 aPoly = [2*tau(t)*sigma_x(s)^2*sigma_v(s2)^4,...
%                              4*tau(t)^2*sigma_x(s)^2*sigma_v(s2)^2+2*tau(t)*r(1)*sigma_v(s2)^4-4*r(2)^2*sigma_v(s2)^4,...
%                              2*tau(t)^3*sigma_x(s)^2+4*tau(t)^2*sigma_v(s2)^2*r(1)-4*tau(t)*r(2)^2*sigma_v(s2)^2,...
%                              2*tau(t)^3*r(1)-tau(t)^2*r(2)^2];
%                 as = roots(aPoly);
%                 
%                 alphas = as(imag(as)==0)*2;
%                 r_opt = zeros([varSize(1),length(alphas)]);
%                 dists = nan([1,length(alphas)]);
%                 
%                 for k = 1:length(alphas)
%                     r_opt(:,k) = r_optOfAlpha(alphas(k));
%                     dists(k) = (r_opt(:,k)'/C)*r_opt(:,k);
%                 end
%                 
%                 wcd(i,t,s,s2) = -sqrt(min(dists))*sign(btnTriggerRule(x(i),v(i),tau(t)));
                
                
%                 fmincon as easy option
                
                options = optimoptions('fmincon','Display','off','Algorithm','sqp','SpecifyObjectiveGradient',true,'SpecifyConstraintGradient',true);
                
                [r_opt,b,~,output] = fmincon(@(x)objFun(x,r,C),zeros(2,1),[],[],[],[],[],[],constr,options);
                
                n_sims(t,s,s2) = n_sims(t,s,s2) + output.funcCount;
                
                wcd(i,t,s,s2) = -sqrt(((r_opt-r)'/C)*(r_opt-r))*sign(ttcTriggerRule(x(i),v(i),tau(t)));
            end
    
            yieldOWCA(t,s,s2) = prod(normcdf(-wcd(bad,t,s,s2)))*(1-prod(normcdf(-wcd(good,t,s,s2))));
        end
    end
end


executionTimeOWCA = toc


figure(1);
volumePlot(sigma_x,tau,sigma_v,yieldOWCA);


function [f,Gf]=objFun(r,r_0,C)
    f = ((r-r_0)'/C)*(r-r_0);
    Gf = (inv(C)+inv(C'))*(r-r_0);
end

function [c,ceq,Gc,Gceq]=constrFun(x,f,Gf)
    c = [];
    ceq = f(x);
    Gc = [];
    if exist('Gf','var')
        Gceq = Gf(x);
    else
        Gceq = [];
    end
end