function setGlobalVars()

    global f_s x_0 v_0 a x_min x_max sigma_x sigma_v tau circle

    f_s = 5e1;  % 1/s
    x_0 = 10;   % m
    v_0 = -10;   % m/s
    a = 10;     % m/s^2

    x_min = 0;
    x_max = .5001;
    sigma_x = linspace(1e-3,1,15);
    sigma_v = linspace(1e-3,1,15);
%     tau = linspace(8,12,15);
    tau = linspace(0.25,0.65,25);

    
    phi = linspace(0,2*pi,9);
    phi = phi(phi~=2*pi);
    circle = [cos(phi);sin(phi)];
    
end