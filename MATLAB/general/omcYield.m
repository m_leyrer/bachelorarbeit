% Get a yield estimate by new method, integrating over a subspace with
% linear, orthogonal boundary hyperplanes
%
% Only works for independent random variables



global f_s x_0 v_0 a x_min x_max sigma_x sigma_v tau

setGlobalVars();

varSize = [2,-ceil(f_s*x_0/v_0/3*2)+3];

ttcTriggerRule = @(x,v,tau)x+v*tau;
ttcTrigger = @(x,v,tau)ttcTriggerRule(x,v,tau)<=0;

btnTriggerRule = @(x,v,tau)x-v.^2/(2*tau);
btnTrigger = @(x,v,tau)btnTriggerRule(x,v,tau)<=0;

[x,v,good,bad] = simulateSystem(x_0,v_0,a,f_s,varSize(2));

pTrig = zeros(varSize(2),length(tau),length(sigma_x),length(sigma_v));

yieldOMC = zeros(length(tau),length(sigma_x),length(sigma_v));


n_mc = 1e6;
err_x = randn([1,n_mc]);
err_v = randn([1,n_mc]);


tic;


for t = 1:length(tau)
    for s = 1:length(sigma_x)
        for s2 = 1:length(sigma_v)

            fprintf("Step: %d:%d:%d, ", t, s, s2);
            t_point=toc;
            
            t_t = tau(t);
            e_x = err_x.*sigma_x(s);
            e_v = err_v.*sigma_v(s2);
            
            for i = 1:varSize(2)
                n_trig = 0;
                for k = 1:n_mc
                    if btnTrigger(x(i)+e_x(k),v(i)+e_v(k),t_t)
                        n_trig = n_trig + 1;
                    end      
                end
                pTrig(i,t,s,s2) = n_trig/n_mc;
            end
            
            yieldOMC(t,s,s2) = prod(1-pTrig(bad,t,s,s2))*(1-prod(1-pTrig(good,t,s,s2)));
            
            fprintf("took %.3f sec.\n", toc-t_point);
        end
    end
end

executionTimeOMC = toc


figure(2);
volumePlot(sigma_x,tau,sigma_v,yieldOMC);
