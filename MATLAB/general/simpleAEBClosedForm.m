global f_s x_0 v_0 a x_min x_max sigma_x sigma_v tau

setGlobalVars();

P = getIntervalProb(x_min, x_max, tau, sigma_x, sigma_v);
% Pnew = getIntervalProbNew(x_min, x_max, tau, sigma_x, sigma_v);

%volumePlot(P)

% PNew = getIntervalProbNew(x_min, x_max, tau, sigma_x, sigma_v);
% 
% % Non-linear constraint function for optimization
% constr = @(x)ndgrid(.99-P(round(x(2)),round(x(1))),0);
% 
% % Find optimum
% x = patternsearch(@(x)-x(1),[10,120],[],[],[],[],[1,1],[50,50],constr);
% sigma_max = sigma(round(x(1)));
% tau_opt = tau(round(x(2)));
% 
% % Plot results
% figure(1);
% surf(sigma,tau,P,'FaceAlpha',.5);
% surf(sigma,tau,PNew,'FaceAlpha',.5);
% xlabel("sigma in m");
% ylabel("tau in s");
% 
% % figure(2);
% % contour(sigma,tau,P,[.99,.9,.8,.7,.6,.5,.4,.3])
% % hold on
% % plot(sigma_max,tau_opt,"r*")
% % xlabel("sigma in m");
% % ylabel("tau in s");

% Calculate the probability to land inside the interval
% [x_min,x_max] for given parameters tau, sigma_x and sigma_v
function p = getIntervalProb(x_min, x_max, tau, sigma_x, sigma_v)
    global f_s x_0 v_0 a
    n_min = max(0, ceil(f_s/v_0*(x_max-x_0+v_0.^2/2/a)));
    n_max = floor(f_s/v_0*(x_min-x_0+v_0.^2/2/a));
    s_x = reshape(sigma_x, [1,1,length(sigma_x)]);
    s_v = reshape(sigma_v, [1,1,1,length(sigma_v)]);
    phis = normcdf(-(x_0+((0:n_max)'/f_s+tau)*v_0)./sqrt(s_x.^2+tau.^2.*s_v.^2));

    ps = zeros([n_max-n_min+1,length(tau),length(sigma_x),length(sigma_v)]);

    for n = n_min:n_max
        i = n-n_min+1;
        ps(i,:,:,:) = prod(1-phis(1:n,:,:,:)).*phis(n+1,:,:,:);
    end
    
    p = reshape(sum(ps,1), [size(ps,2), size(ps,3), size(ps,4)]);
    
end

% doesn't work correctly!!!!
function p = getIntervalProbNew(x_min, x_max, tau, sigma_x, sigma_v)
    global f_s x_0 v_0 a
    n_min = max(0, ceil(f_s/v_0*(x_max-x_0+v_0.^2/2/a)));
    n_max = floor(f_s/v_0*(x_min-x_0+v_0.^2/2/a));
    s_x = reshape(sigma_x, [1,1,length(sigma_x)]);
    s_v = reshape(sigma_v, [1,1,1,length(sigma_v)]);
    phis = normcdf((x_0+((0:n_max)'/f_s+tau)*v_0)./sqrt(s_x.^2+tau.^2.*s_v.^2));
    
    
    p = reshape(prod(phis(1:n_min-1,:,:,:),1).*(1-prod(phis(n_min:n_max,:,:,:),1)), [size(phis,2) size(phis,3), size(phis,4)]);
    
end
