%----------------------------------------------------------------------
% * Identify class
%
%
\NeedsTeXFormat{LaTeX2e}[1999/12/01]
\ProvidesClass{msvexam}%
    [2016/02/12 v0.1 %
    msvexam.cls MSV Exam class]%
%
%----------------------------------------------------------------------
% * Key-Value Options
%
\RequirePackage{kvoptions}%
%
%----------------------------------------------------------------------
% * Class options
%
% solution boolean
\newif\ifsolution
\solutionfalse
%
% grid boolean
\newif\ifgrid
\gridfalse
% subproblembox boolean
\newif\ifshowbox
\showboxtrue

%
\SetupKeyvalOptions{family=msvexam,prefix=msvexam@}%
%
\newcommand\msvexam@ptsize{1}
\DeclareOption{10pt}{\renewcommand\msvexam@ptsize{0}}
\DeclareOption{11pt}{\renewcommand\msvexam@ptsize{1}}
\DeclareOption{12pt}{\renewcommand\msvexam@ptsize{2}}
\ExecuteOptions{12pt} % make 12pt standard text size
%
% msvcommon.sty
\DeclareVoidOption{ngerman}{\PassOptionsToPackage{ngerman}{msvcommon}}%
\DeclareVoidOption{german}{\PassOptionsToPackage{german}{msvcommon}}%
\PassOptionsToPackage{cmyk}{msvcommon}
%
% msvexam.cls
\DeclareOption{solution}{\solutiontrue}
\DeclareOption{grid}{\gridtrue}
\DeclareBoolOption{sftitle}
\DeclareComplementaryOption{rmtitle}{sftitle}%
\DeclareBoolOption{sfheads}
\DeclareComplementaryOption{rmheads}{sfheads}%
%
% Process options
\PassOptionsToClass{a4paper,twoside}{article}
\DeclareOption*{}
\DeclareDefaultOption{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions*\relax
\ProcessKeyvalOptions*\relax
%
%----------------------------------------------------------------------
% * Load base class report and required packages
%
\LoadClass[1\csname msvexam@ptsize \endcsname pt]{article}
%
\RequirePackage{geometry}  % Layout
\RequirePackage{ifthen}    % \ifthenelse command
\RequirePackage{etoolbox}  % for \ifstrequal
\RequirePackage{msvcommon} % TUM Colors, Logos, MSV Commands
\RequirePackage{nextpage}  % used to remove page style
\RequirePackage{graphicx}  % include graphics
\RequirePackage{xkeyval}   % key-value options
\RequirePackage{calc}
\RequirePackage{tikz}
\usetikzlibrary{calc}
%
\ifanypdf
  \RequirePackage{microtype}
\fi
%
%----------------------------------------------------------------------
% * Title Page
%
\newcommand*\msvexam@footerfont{\fontsize{11}{11}\selectfont}
\newlength\msvexam@msvlogosize
\setlength\msvexam@msvlogosize{12mm}
\newlength\msvexam@msvlogofontsize
\setlength\msvexam@msvlogofontsize{11pt}
\newlength\msvexam@msvlogobaselineskip
\setlength\msvexam@msvlogobaselineskip{12pt}
%
\newcommand{\examnotes}{
  \begin{itemize}\setlength\itemsep{0.5ex}
    \ifenglish{
      \item This problem booklet contains \total{page} pages.
      \item A total of \total{TotalScore} credits can be achieved.
    }{
      \item Dieses Aufgabenheft hat \total{page} Seiten.
      \item Die Gesamtzahl der Punkte betr\"agt \total{TotalScore}.
    }
  \end{itemize}
}
\newcommand{\examextranotes}{
    \begin{itemize}\setlength\itemsep{0.5ex}
    \ifenglish{
      \item You may use up to 5 two-sided or 10 one-sided DIN A4 sheets with arbitrary notes.
      \item Otherwise the exam is closed book. 
      \item The use of electronic devices such as calculators, cell phones, notebooks, and similar devices is not allowed.
      \item Subproblems marked with * can be solved independently of previous subproblems.
      \item {\bfseries Points will only be given if it is clear how the solutions have been obtained!}
    }{
      \item Als Unterlagen f\"ur die Pr\"ufung sind maximal 5 beliebig beschriebene oder 10 einseitig beschriebene Bl\"atter DIN A4 erlaubt.
      \item Taschenrechner und Mobiltelefone sind nicht zugelassen.
      \item Mit * gekennzeichnete Aufgaben sind ohne Kenntnis des Ergebnisses der
            vorhergehenden Teilaufgaben l\"osbar.
      \item {\bfseries Es werden nur solche Ergebnisse gewertet, 
            bei denen der L\"osungsweg erkennbar ist!}
    }
  \end{itemize}
}
%
\newif\iftime
\timefalse
\newcommand{\@time}{\mbox{}}
\newcommand{\@room}{\mbox{}}
\newcommand{\examtime}[1]{\renewcommand{\@time}{#1}\timetrue}
\newcommand{\examroom}[1]{\renewcommand{\@room}{#1}}
\renewcommand\maketitle{%
\begin{titlepage}%
\let\footnoterule\relax%
\let\footnote\thanks%
\ifmsvexam@sftitle\sffamily\fi
\fontsize{12}{14}%
\selectfont%
\setlength\parindent{0mm}%
\begin{center}%
	{\LARGE\punktetabelle~\\~\\%
	\examname}%
	~\\~\\~\\%
	{\LARGE\bfseries%
	\hspace*{-0.1\textwidth}\parbox{1.2\textwidth}{\centering\@title}}\\[2mm]% allow title to protrude into margins
	{\large\@author}\\[5mm]%
	{\large \@date\\%
	\iftime\@time\fi\\
	\@room}
\end{center}
\ifsolution
\begin{center}\LARGE\bfseries\color{blue}\solutionname\end{center}
\else
\begin{center}\LARGE\bfseries\phantom{\solutionname}\end{center}
\fi
\centering
\parbox{.66\textwidth}{
	\lastname:\dotfill\\[5mm]
	\firstname:\dotfill\\[5mm]
	\matrikelname:\dotfill\\[5mm]
	\roomname:\dotfill\\[5mm]
	\platzname:\dotfill}
\vfill
\examnotes
\examextranotes
{\msvexam@footerfont%
{\hrule height 0.252mm\vskip 0.5\msvexam@msvlogosize}%\vskip 0.5\msvexam@msvlogosize\vskip 0.252mm
\noindent%
\rlap{\EIlogo{height=\msvexam@msvlogosize,color=black}}%
\vbox{\fontsize{\msvexam@msvlogofontsize}{\msvexam@msvlogobaselineskip}\selectfont\centering\EIname\\\MSVname\\\MSVhead}%
\llap{\MSVlogo{height=\msvexam@msvlogosize,color=black}}%
}%
\end{titlepage}%
}
%
%----------------------------------------------------------------------
% * Introduce commands for exam
%
\newcommand{\Haken}{{\color{red}\ensuremath{\;\surd\;}}}
\newcommand{\info}[1]{{#1}}
\newcommand{\highlight}[1]{\par\msvtikzbox[fill=black!15,draw=none]{#1}\par}
%
%----------------------------------------------------------------------
% * Problem boxes and counters copied from Stephan Guenther's style
%
\RequirePackage{framed}
\RequirePackage{totcount}
\def\use#1#2{\csname#1#2\endcsname}
\def\set#1#2#3{\expandafter\def\csname#1#2\endcsname{#3}}
\newcommand{\msvexam@creditlabel}{\ifenglish{points}{Punkte}}
\newcommand{\msvexam@problemlabel}{\ifenglish{Problem}{Aufgabe}}
\newcounter{ProblemCounter}
\newcounter{SubProblemCounter}[ProblemCounter]
\renewcommand{\theSubProblemCounter}{\alph{SubProblemCounter})}
\newcommand\thisproblemscore[0]{Problem\arabic{ProblemCounter}Score}
%
\newcounter{TotalScore}
\newcounter{\thisproblemscore}
\regtotcounter{page}
\regtotcounter{ProblemCounter}
\regtotcounter{TotalScore}
\newtotcounter{TOTC1}
\newtotcounter{TOTC2}
\newtotcounter{TOTC3}
\newtotcounter{TOTC4}
\newtotcounter{TOTC5}
\newtotcounter{TOTC6}
\newtotcounter{TOTC7}
\newtotcounter{TOTC8}
\newtotcounter{TOTC9}
\newtotcounter{TOTC10}
\set{cws}{1}{TOTC1}
\set{cws}{2}{TOTC2}
\set{cws}{3}{TOTC3}
\set{cws}{4}{TOTC4}
\set{cws}{5}{TOTC5}
\set{cws}{6}{TOTC6}
\set{cws}{7}{TOTC7}
\set{cws}{8}{TOTC8}
\set{cws}{9}{TOTC9}
\set{cws}{10}{TOTC10}
%
\newcommand\counterlabel[1]{
	\addtocounter{#1}{-1}
	\refstepcounter{#1}\label{ctr:#1}
}
\newcommand\counterref[1]{\ref{ctr:#1}}
%
\newcommand{\punktetabelle}[1][1.5]{%
\ifnum\totvalue{ProblemCounter}>0
\begin{tikzpicture}
  \draw (0,0) -- ++(\totvalue{ProblemCounter}*#1+1*#1,0);
		\foreach \a in {1,...,\totvalue{ProblemCounter}} {
      \draw (\a*#1,1) -- ++(0,-2) ++(-0.5*#1,1.5) node{\a};
		}
		\draw (\totvalue{ProblemCounter}*#1+#1,1) ++(-0.5*#1,-0.5) node{$\sum$};
    \ifsolution
		    \foreach \a in {1,...,\totvalue{ProblemCounter}} {
          \draw (\a*#1,1) ++(-0.5*#1,-1.5) node{{\color{blue}\total{\use{cws}{\a}}}};
        }
		  \draw (\totvalue{ProblemCounter}*#1+#1,1) ++(-0.5*#1,-1.5) node{{\color{blue}\total{TotalScore}}};
    \fi
\end{tikzpicture}
\fi
}
%
\newcommand{\problem}[1]{
  \clearpage
	\counterlabel{\thisproblemscore}  % save score of last problem
  \stepcounter{ProblemCounter}%
	\newcounter{\thisproblemscore}%
	{%
    \creditbox{\total{\use{cws}{\theProblemCounter}}}
		{%
			\msvexam@problemfont\msvexam@problemlabel{} \arabic{ProblemCounter}%
		}%
		\msvexam@problemsubfont\quad#1 (\total{\use{cws}{\theProblemCounter}} \msvexam@creditlabel)%
	}%
	\setcounter{\use{cws}{\theProblemCounter}}{0}%
	\vskip.5\baselineskip%
}%
%
\newcommand{\creditbox}[1]{
  \marginpar{\fboxrule1pt\framebox[8mm]{\rule[-1.5mm]{0pt}{6mm}\ifsolution\color{blue} #1\fi}}
}
%
\newlength\@subproblemheight
\define@key{subproblem}{points}{\def\@subproblempoints{#1}}
\define@key{subproblem}{height}{\setlength\@subproblemheight{#1}}
\define@key{subproblem}{mark}{\def\@subproblemmark{#1}}
\newcommand{\subproblemx}[3][]{
  \setkeys{subproblem}{points=1}
  \setkeys{subproblem}{height=2cm}
  \setkeys{subproblem}{mark=*}
  \setkeys{subproblem}{#1}
  \@subproblem{#2}{#3}
}
%
\newcommand{\subproblem}[3][]{
  \setkeys{subproblem}{points=1}
  \setkeys{subproblem}{height=2cm}
  \setkeys{subproblem}{mark=}
  \setkeys{subproblem}{#1}
  \@subproblem{#2}{#3}
}
\newcommand{\subproblemnobox}[3][]{
  \showboxfalse
  \subproblem[#1]{#2}{#3}
  \showboxtrue
}
\newcommand{\subproblemxnobox}[3][]{
  \showboxfalse
  \subproblemx[#1]{#2}{#3}
  \showboxtrue
}
%
\newcommand{\@subproblem}[2]{
		\refstepcounter{SubProblemCounter}%
		\addtocounter{\thisproblemscore}{\@subproblempoints}%
		\addtocounter{TotalScore}{\@subproblempoints}%
		\addtocounter{\use{cws}{\theProblemCounter}}{\@subproblempoints}%
    \smallskip
		\alph{SubProblemCounter})\@subproblemmark\;\;%
		\creditbox{\@subproblempoints}
    #1\par
    \vskip1ex
    \ifshowbox
      \msvtikzbox[height=\@subproblemheight]{\ifsolution {\large #2} \else \hfil \fi}
    \else
      {\ifsolution {\large #2} \else \hfil \fi}
    \fi
    \par
}%
%
\newlength\@msvtikzboxheight
\define@key{msvtikzbox}{fill}{\def\@msvtikzboxbgcolor{#1}}
\define@key{msvtikzbox}{draw}{\def\@msvtikzboxfgcolor{#1}}
\define@key{msvtikzbox}{height}{\setlength\@msvtikzboxheight{#1}}
%
\newcommand{\msvtikzbox}[2][]{%
  \setkeys{msvtikzbox}{fill=none}
  \setkeys{msvtikzbox}{draw=black}
  \setkeys{msvtikzbox}{height=0cm}
  \setkeys{msvtikzbox}{#1}
  \@msvtikzbox{#2}%
}
%
\newcommand{\@msvtikzbox}[1]{
	\begin{tikzpicture}
	\ifsolution\else\ifgrid\draw[black!25,step=.5cm] (-.5\textwidth,-.5\@msvtikzboxheight+1pt) grid (.5\textwidth,.5\@msvtikzboxheight-1pt);\fi\fi
	\node [rectangle,
	inner sep=10pt, inner ysep=14pt,
	draw=\@msvtikzboxfgcolor,
	line width=1pt,
	fill=\@msvtikzboxbgcolor,
	minimum height=\@msvtikzboxheight-2pt]{
		\begin{minipage}{\textwidth-20pt} % 20pt: 2 x inner sep=10pt
		#1
		\end{minipage}
	};
	\end{tikzpicture}%
}
%
%----------------------------------------------------------------------
% * Geometry and Style
%
% Ueberschriften
\newcommand\msvexam@headsfamily{\rmfamily}
\ifmsvexam@sfheads
    \renewcommand\msvexam@headsfamily{\sffamily}
\fi
\newcommand{\msvexam@problemfont}{\msvexam@headsfamily\Large\bfseries}
\newcommand{\msvexam@problemsubfont}{\msvexam@headsfamily\large\bfseries}
%
%----------------------------------------------------------------------
% Page styles
%
% If two-side layout: if page before new chapter does not contain text,
%                     leave blank (no header and footer)
\ifthenelse{\boolean{@twoside}}{%
\renewcommand{\cleardoublepage}{\cleartooddpage[\thispagestyle{empty}]}%
}{%
\renewcommand{\cleardoublepage}{\clearpage}%
}
%
% Satzspiegel
\geometry{left=2cm,right=2cm,top=1cm,bottom=1cm,includeheadfoot,heightrounded}
\parindent0pt
\parskip1mm
\marginparwidth8mm
\marginparsep3mm
%
% Definitionen
\newcommand{\matrikelname}{\ifenglish{Immatriculation No.}{Matrikel-Nr.}}
\newcommand{\platzname}{\ifenglish{Seat No.}{Platz-Nr.}}
\newcommand{\firstname}{\ifenglish{First name}{Vorname}}
\newcommand{\lastname}{\ifenglish{Last name}{Nachname}}
\newcommand{\roomname}{\ifenglish{Room No.}{Raum-Nr.}}
\newcommand{\solutionname}{\ifenglish{Solution}{L\"osung}}
\newcommand{\examname}{\ifenglish{Exam}{Pr\"ufung}}
%
% Kolumnentitel:
\def\msvexam@header{\makebox[80mm][l]{Name:
\dotfill\quad}\makebox[60mm][l]{\matrikelname: \dotfill}}
%
% Kopf- und Fusszeile:
\if@twoside\def\ps@headings{\let\@mkboth\markboth
 \def\@oddfoot{}
 \def\@evenfoot{}
 \def\@evenhead{\strut\thepage\hfill}
 \def\@oddhead{\msvexam@headsfamily\strut\msvexam@header\hfill \thepage}
}
 \else 
 \def\ps@headings{\let\@mkboth\markboth
 \def\@oddfoot{}
 \def\@evenfoot{}
 \def\@oddhead{\msvexam@headsfamily\strut\msvexam@header\hfill \thepage}
}
\fi
\pagestyle{headings}%
