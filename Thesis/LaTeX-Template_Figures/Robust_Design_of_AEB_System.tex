\documentclass[conference]{IEEEtran}
\usepackage[T1]{fontenc}
\usepackage{cite}
\ifCLASSINFOpdf
\else
\fi
\usepackage{url}
\hyphenation{}
\usepackage{amsbsy}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[caption=false,font=footnotesize]{subfig}





\newlength\myindent
\setlength\myindent{1em}
\newcommand\bindent{
  \begingroup
  \setlength{\itemindent}{\myindent}
  \addtolength{\algorithmicindent}{\myindent}
}


\usepackage{tikz}
\usetikzlibrary{arrows,decorations,backgrounds,shadows,plotmarks,shapes}

\usepackage{pgfplots}
\usepgfplotslibrary{colormaps,fillbetween}
\pgfplotsset{compat=newest}
\pgfplotsset{plot coordinates/math parser=false}
\pgfplotsset{every axis/.append style={font=\footnotesize}}
\newlength\figureheight
\newlength\figurewidth
\setlength\figureheight{4cm}
\setlength\figurewidth{7.3cm}

\usepackage{3dplot}

\definecolor{TUMblue}{rgb}{0,0.396,.741}
\definecolor{NWSred}{rgb}{0.6,0,0}
\definecolor{TUMgray}{rgb}{.9,.9,.9}
\definecolor{Mygreen}{rgb}{0,.7,0}
\definecolor{Darkred}{rgb}{0.753,0,0}

\DeclareMathOperator*{\h}{H}
\DeclareMathOperator*{\T}{T}
\DeclareMathOperator*{\argmin}{argmin}
\DeclareMathOperator*{\argmax}{argmax}
\DeclareMathOperator*{\Diag}{diag}
\DeclareMathOperator*{\Blkdiag}{blkdiag}
\DeclareMathOperator*{\Mod}{mod}
\DeclareMathOperator*{\sort}{sort}
\DeclareMathOperator{\Quant}{Q}
\DeclareMathOperator{\E}{E}
\DeclareMathOperator*{\Var}{Var}
\DeclareMathOperator*{\Cov}{Cov}
\DeclareMathOperator*{\trace}{tr}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\SNR}{SNR}
\DeclareMathOperator{\OSNR}{OSNR}
\DeclareMathOperator{\SER}{SER}
\DeclareMathOperator{\BER}{BER}
\DeclareMathOperator{\MSE}{MSE}
\DeclareMathOperator{\dB}{dB}
\DeclareMathOperator{\e}{e}
\DeclareMathOperator{\Pe}{P_e}
\DeclareMathOperator{\supp}{supp}
\DeclareMathOperator{\spark}{spark}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\corr}{c}
\DeclareMathOperator{\vectorize}{vec}
\DeclareMathOperator{\normcdf}{\Phi}



\begin{document}

Fig.~\ref{fig:setting} depicts the general setting in which a vehicular safety function is embedded.



\begin{figure}
	\centering
	\begin{tikzpicture}
		\draw[->] (-2.1,0) -- (6.5,0);
		\fill[violet!30] (1.45,0) rectangle (3.2,1);
		\draw[violet,<->,thick] (1.45,0) -- (3.2,0);
		\draw[dashed] (0,-0.1) node[below]{$x_\text{ego}\left(t\right)$} -- (0,1);
		\draw[dashed] (3.25,-0.1) node[below]{$x_\text{obj}\left(t\right)$} -- (3.25,1);
		\draw[->] (0,0.5) -- ++(0.3,0) node[right]{$v_\text{ego}\left(t\right)$};
		\draw[->] (5.25,0.5) -- ++(0.15,0) node[right]{$v_\text{obj}\left(t\right)$};
		\node[inner sep=0pt] (ego-vecicle) at (-1,0.5) {\includegraphics[width=2cm]{Figures/Audi_A3_Sedan_blue.pdf}};
		\node[inner sep=0pt] (object) at (4.25,0.5) {\includegraphics[width=2cm]{Figures/Audi_A3_Sedan_red.pdf}};
		\node[above] at (-1,1) {ego vehicle};
		\node[above] at (4.25,1) {object};
		\node[align=center,violet] at (2.325,0.5) {acceptable\\interval};
	\end{tikzpicture}
	\caption{Considered scenario at time $t$.}
	\label{fig:scenario}
\end{figure}



\begin{figure}
	\centering
	\begin{tikzpicture}
		\node[rectangle,draw,text width=1.5cm,minimum height=0.5cm,text centered,TUMblue] (sensors)  at (1.5,0) {sensors};
		\node[rectangle,draw,text width=1.5cm,minimum height=0.5cm,text centered,Darkred] (function) at (4.5,0) {function};
		
		\draw[-latex] (0,0) node[left]{$\boldsymbol{x}\left[n\right]$} -- (sensors);
		\draw[-latex] (1.5,-0.75) node[below]{$\boldsymbol{\varepsilon}\left[n\right]$} -- (sensors);
		\draw[-latex] (sensors) -- node[midway,above]{$\boldsymbol{y}\left[n\right]$} (function);
		\draw[-latex] (function) -- (6,0) node[right]{$f\left(\boldsymbol{y}\left[n\right];\boldsymbol{\theta}\right)$};
	\end{tikzpicture}
	\caption{General mathematical model of a vehicular safety system including sensor measurement errors.}
	\label{fig:system_model}
\end{figure}



\begin{figure}
	\centering
	\subfloat[]{
		\input{Figures/Plots/results/probability_specification_fulfillment_vs_sigma.tikz}
	}
	\qquad
	\subfloat[]{
		\input{Figures/Plots/results/probability_specification_fulfillment_vs_tau.tikz}
	}
	\caption{Probability that the specification $x_\mathrm{min} \leq x_{\mathrm{end}} \leq x_\mathrm{max}$ is fulfilled vs. $\sigma$ (a) and $\tau$ (b) for $x_0 = 10\,\mathrm{m}$, $v_0 = -10\,\frac{\mathrm{m}}{\mathrm{s}}$, $f_\mathrm{s} = 1\,\mathrm{kHz}$, $a = 10\,\frac{\mathrm{m}}{\mathrm{s}^2}$, $x_\text{min}=0$ and $x_\text{max}=0.5\,\mathrm{m}$ with solid lines and crosses representing theoretical and simulation results, respectively.}
	\label{fig:probability_specification_fulfillment}
\end{figure}



\begin{figure}\centering
	\input{Figures/Plots/results/contour_lines_probability_specification_fulfillment.tikz}
	\vspace{-3mm}
	\caption{Contour lines of $P\left(x_\mathrm{min} \leq x_{\mathrm{end}} \leq x_\mathrm{max}\right)$ at different heights $p$ for $x_0 = 10\,\mathrm{m}$, $v_0 = -10\,\frac{\mathrm{m}}{\mathrm{s}}$, $f_\mathrm{s} = 1\,\mathrm{kHz}$, $a = 10\,\frac{\mathrm{m}}{\mathrm{s}^2}$, $x_\text{min}=0$ and $x_\text{max}=0.5\,\mathrm{m}$ with the optimal parameter value pair $\left(\sigma_\text{max},\tau_\text{opt}\right)$ determined by the joint function and sensor design for $P_\text{min}=0.99$.}
	\label{fig:contour_lines_probability_specification_fulfillment}
\end{figure}

\end{document}


