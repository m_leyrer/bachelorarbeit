#!/bin/bash

FILES="images/scenario1.eps images/scenario2.eps images/TUMUhrenturm.png images/TUMUhrenturmBW.png msvbeamer.cls msvcommon.sty msvfonts.sty msvposter.cls msvpresentation.sty msvreport.cls msvsymbols.sty random.sty template_poster_portrait.tex template_poster_landscape.tex template_presentation.tex template_thesis.tex examplebibfile.bib IEEEtran.bst IEEEabrv.bib"
DIRNAME="msvlatex_student_files"

if [ -d $DIRNAME ]; then
    rm -rf $DIRNAME
fi
mkdir $DIRNAME
for file in $FILES; do
    rsync -R $file $DIRNAME/
done;
if [ -f $DIRNAME.zip ]; then
    rm $DIRNAME.zip
fi
zip $DIRNAME.zip -r $DIRNAME/
