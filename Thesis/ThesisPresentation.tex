\documentclass[10pt,t]{beamer}
\usepackage{msvcommon}

% -------------------------------------------------------------------------------
% ** Input Encoding
% - utf8x is recommended at MSV
% - Some comments on encoding can be found here:
%   https://groups.google.com/forum/?fromgroups=#!msg/comp.text.tex/4LC-xODb-LU/1Bd5UZOMNM4J
%
\ifxetexorluatex
  % XeTeX and LuaTeX support utf8 by default
\else
  % File encoding utf8x
  \usepackage{ucs}
  \usepackage[utf8x]{inputenc}
\fi
%
%--------------------------------------------------------------------------------
% ** Language Settings and Hyphenation Patterns
%
% Note: final argument to babel sets the main language
%
\ifxetex%
  \usepackage{polyglossia}
  \setmainlanguage{english}
  \setotherlanguage{german}
%  \setmainlanguage{german}
%  \setotherlanguage{english}
\else
  \usepackage[english,ngerman]{babel} 
\fi


\usepackage{bibentry}

%--------------------------------------------------------------------------------
% ** MSV style defintions for beamer presentations
%
\usepackage[colorblock]{msvpresentation}
%-------------------------------------------------------------------------------
% ** Options for msvpresentation (all options are disabled by default)
% 1) boolean options for msvpresentation package:
%  | bw             | black and white color scheme                             |
%  | colorblock     | use non-white background colors for blocks               |
%  | frameblock     | use colored borders for blocks                           |
%
%--------------------------------------------------------------------------------
% ** Setup look of presentation
%
% MSV style should be compatible with the beamer themes
%
\useinnertheme{default}
\useoutertheme{default}

% Colors:
%\usecolortheme[named=TUMDarkerBlue]{structure} % variant: TUMBlack
\usecolortheme[named=TUMBlack]{structure} % variant: TUMBlack
\setbeamercolor{frametitle}{fg=TUMBlack} % variant: TUMDarkerBlue
\setbeamercolor{normal text}{fg=TUMBlack,bg=TUMWhite} % font/background
\setbeamercolor{alerted text}{fg=TUMBeamerRed,bg=TUMWhite} % alert boxes
\setbeamercolor{example text}{fg=TUMBeamerGreen,bg=TUMWhite} % example boxes

% Headers, Footers, Titlepage:
%   Note: can adjust height of the frame title with \raisetitle and \lowertitle
\setbeamertemplate{headline}[tum] % no, tum, msvtum
\setbeamertemplate{footline}[authortitle] % minimal, author, authortitle
\setbeamertemplate{title page}[tum][left] % with clock tower
%\setbeamertemplate{title page}[default][left,sep=0pt] % without clock tower

%--------------------------------------------------------------------------------
% ** Presentation title, author, etc.
%
\title{Simulation-Based Robust Design of an Automatic Emergency Braking System Considering Sensor Measurement Errors}
\author[Michael L. Leyrer (TUM)]{Michael L. Leyrer}
\institute{\MSVname}
\date{\today}

\begin{document}
{ % use custom header/footer for title page
\setbeamertemplate{headline}[tum]
\setbeamertemplate{footline}[no]
\begin{frame}
\titlepage
\end{frame}
}

\section{Motivation}

\begin{frame}
	\frametitle{Motivation (1)}
	
	%	Car manufacturers implement vehicular safety functions which improve passenger safety. As those rely on sensor measurements to interpret the driving situation, random sensor measurement errors have to be taken into account when designing a safety system.
	\vspace{.5cm}
	Vehicular safety systems improve passenger safety but are subject to measurement errors
	
	\vspace{1cm}
	
	\begin{center}
		\begin{tikzpicture}
		\node[rectangle,draw,text width=1.5cm,minimum height=0.5cm,text centered,blue] (sensors)  at (1.5,0) {sensors};
		\node[rectangle,draw,text width=1.5cm,minimum height=0.5cm,text centered,red] (function) at (5.5,0) {function};
		\node[rectangle,draw,text width=1.5cm,minimum height=0.5cm,text centered,green] (actor) at (8.5,0) {action};
		
		\draw[-latex] (0,0) node[left]{situation} -- (sensors);
		\draw[-latex](1.5,-0.75) node[below]{errors} -- (sensors);
		\draw[-latex](sensors) -- node[midway,above]{measurement} (function);
		\draw[-latex](function) -- node[above,midway]{decision} (actor);
		\end{tikzpicture}
	\end{center}
	\vspace{.7cm}
	%	Choosing arbitrarily good sensor is not a good design solution because very good sensor cost a lot. Therefore the task in designing a robust system is to minimize cost while still achieving a minimal quality measure when the function parameters are optimal. This can be formulated as \cite{StoeckleAEB},\cite{StoeckleAEB2}
	
	Robust design is needed:
	
	\begin{itemize}
		\item System designed such that it meets certain requirements in a robust manner despite sensor measurement errors
		\item Can be formulated as an optimization problem using a quality measure
	\end{itemize}
	%	\begin{equation}\label{eq:task}
	%		(\boldsymbol{\tau}_{\text{opt}},\boldsymbol{\sigma}_{\text{opt}})=\underset{\boldsymbol{\tau},\boldsymbol{\sigma}}{\arg\min}\:c(\boldsymbol{\sigma})\quad \text{s.t.}\quad Q(\boldsymbol{\tau},\boldsymbol{\sigma})\geq Q_{\text{min}}
	%	\end{equation}
\end{frame}

\begin{frame}
	\frametitle{Motivation (2)}
	
	\vspace{1cm}
	
	The quality measure is often probability which has to be evaluated by simulating the system
	
	\vspace{1cm}
	Methods like Monte Carlo~(MC) simulation are computationally expensive
	
	\vspace{1cm}
	
	\begin{block}{Task}
		Find a computationally less expensive method for evaluating probabilities in the context of vehicular safety systems.
	\end{block}
	%	The quality measure chosen is defined as the probability of fulfilling a certain performance specification. In order to solve \eqref{eq:task} one has to calculate this probability, which can often only be done by simulating the system at hand.\\
	%	
	%	\bigskip
	%	
	%	The naive approach would be a Monte Carlo~(MC) simulation of the system, but this method involving many simulation runs with different realizations of the random measurement errors is very computationally expensive.
	%	
	%	\bigskip
	%	
	%	\begin{block}{}
	%		Finding a less computationally expensive method can significantly speed up the design process of vehicular safety systems.
	%	\end{block}
	
\end{frame}

\section{System Model}

\begin{frame}
	\frametitle{System Model}
	
	%	The test function used here is automatic emergency braking~(AEB). It triggers a braking maneuver if a dangerous situation is detected.
	%	
	Considered scenario for time $t$:
	
	\begin{center}
		\begin{tikzpicture}
		\draw[->] (-2.1,0) -- (6.5,0);
		%		\fill[violet!30] (1.45,0) rectangle (3.2,1);
		%		\draw[violet,<->,thick] (1.45,0) -- (3.2,0);
		\draw[dashed] (0,-0.1) node[below]{$x_\text{ego}\left(t\right)$} -- (0,1);
		\draw[dashed] (3.25,-0.1) node[below]{$x_\text{obj}\left(t\right)$} -- (3.25,1);
		\draw[->] (0,0.5) -- ++(0.3,0) node[right]{$v_\text{ego}\left(t\right)$};
		\draw[->] (5.25,0.5) -- ++(0.15,0) node[right]{$v_\text{obj}\left(t\right)$};
		\node[inner sep=0pt] (ego-vecicle) at (-1,0.5) {\includegraphics[width=2cm]{LaTeX-Template_Figures/Figures/Audi_A3_Sedan_blue.pdf}};
		\node[inner sep=0pt] (object) at (4.25,0.5) {\includegraphics[width=2cm]{LaTeX-Template_Figures/Figures/Audi_A3_Sedan_red.pdf}};
		\node[above] at (-1,1) {ego vehicle};
		\node[above] at (4.25,1) {object};
%		\node[align=center,violet] at (2.325,0.5) {acceptable\\interval};
		\end{tikzpicture}
	\end{center}
	\vspace{.4cm}
	
	Relative position and velocity are sampled with a fixed frequency $f_\text{s}$, i.e., $t_n=\frac{n}{f_\text{s}}$:
	
	\begin{equation}
	\begin{bmatrix}
	x_n\\v_n
	\end{bmatrix} = 
	\begin{bmatrix}
	x(t_n)\\v(t_n)
	\end{bmatrix} = 
	\begin{bmatrix}
	x_{\text{obj}}(t_n) - x_{\text{ego}}(t_n)\\v_{\text{obj}}(t_n) - v_{\text{ego}}(t_n)
	\end{bmatrix}
	\end{equation}
	
	\vspace{.6cm}
	Both are measured with errors which are assumed to be Gaussian: 
	\begin{equation}
	\begin{bmatrix}
	\hat{x}_n\\\hat{v}_n
	\end{bmatrix}=\begin{bmatrix}
	x_n\\v_n
	\end{bmatrix} + \begin{bmatrix}
	\varepsilon_{x,n}\\\varepsilon_{v,n}
	\end{bmatrix},\quad \varepsilon_{x,n}\sim\mathcal{N}(0,\sigma_x^2),\: \varepsilon_{v,n}\sim\mathcal{N}(0,\sigma_v^2)
	\end{equation}
	
\end{frame}

\begin{frame}
	\frametitle{System Model - Triggering Rules}
	\begin{itemize}
		\item Decision rule based on time-to-collision~(TTC) $-\frac{\hat{x}_n}{\hat{v}_n}$ with a threshold $\tau$:\\Trigger a braking maneuver in time step $n$ if
		
		\begin{equation}
		\hat{x}_n\leq -\tau\hat{v}_n.
		\end{equation}
		
		\vspace{.2cm}
		
		$\Rightarrow$ linear decision boundary in the $\hat{x}_n$-$\hat{v}_n$-space.
		
		\bigskip
		
		\item Decision rule based on brake-threat-number~(BTN) $\frac{\hat{v}^2_n}{2\hat{x}_n}$ with a threshold $\tau$:\\Trigger a braking maneuver if
		
		\begin{equation}
		\hat{x}_n\leq \frac{\hat{v}_n^2}{2\tau}
		\end{equation}
		
		\vspace{.2cm}
		
		$\Rightarrow$ non-linear decision boundary in the $\hat{x}_n$-$\hat{v}_n$-space.
	\end{itemize}
\end{frame}

%\begin{frame}
%	\frametitle{System Model - Simulation Parameters}
%	\vspace{.3cm}
%	The following parameters were used in system simulations:
%	
%	\vspace{.5cm}
%	
%	
%
%	\vspace{.5cm}
%	
%	$f_\text{s}$ is kept variable due to its influence on computational effort
%	
%\end{frame}

\begin{frame}
	\frametitle{Robust Design}
	\begin{equation}
		(\tau_\text{opt},\sigma_{x,\text{opt}},\sigma_{v,\text{opt}})=\arg\min_{\tau,\sigma_x,\sigma_v}\:c\quad\text{s.t.}\quad Q\geq Q_\text{min}
	\end{equation}
	
		\vspace{.2cm}
	
	$c$ : cost function, e.g., $c=-(\sigma_x + \sigma_v)$
	
	\vspace{.2cm}
	
	$Q_\text{min}$ : required quality level
	
	\vspace{.2cm}
	
	$Q$ : probability $\mathsf{P}(x_\text{min}\leq x_\text{end}\leq x_\text{max})$ that final distance between ego vehicle and object after the braking maneuver is in $[x_\text{min},x_\text{max}]$
	
	\begin{center}
		\begin{tikzpicture}
		\draw[->] (-2.1,0) -- (6.5,0);
				\fill[violet!30] (1.45,0) rectangle (3.2,1);
				\draw[violet,<->,thick] (1.45,0) -- (3.2,0);
		\draw[dashed] (0,-0.1) node[below]{$x_\text{ego}\left(t\right)$} -- (0,1);
		\draw[dashed] (3.25,-0.1) node[below]{$x_\text{obj}\left(t\right)$} -- (3.25,1);
		\draw[->] (0,0.5) -- ++(0.3,0) node[right]{$v_\text{ego}\left(t\right)$};
		\draw[->] (5.25,0.5) -- ++(0.15,0) node[right]{$v_\text{obj}\left(t\right)$};
		\node[inner sep=0pt] (ego-vecicle) at (-1,0.5) {\includegraphics[width=2cm]{LaTeX-Template_Figures/Figures/Audi_A3_Sedan_blue.pdf}};
		\node[inner sep=0pt] (object) at (4.25,0.5) {\includegraphics[width=2cm]{LaTeX-Template_Figures/Figures/Audi_A3_Sedan_red.pdf}};
		\node[above] at (-1,1) {ego vehicle};
		\node[above] at (4.25,1) {object};
		\node[align=center,violet] at (2.325,0.5) {acceptable\\interval};
		\end{tikzpicture}
	\end{center}

	\bigskip
	
	In general, $Q$ can only be evaluated by system simulations
\end{frame}

\begin{frame}
	\frametitle{Monte Carlo (MC) Simulation}
	
	
	\vspace{.5cm}
	
	Approximating a probability $\mathsf{P}(f(\mathsf{s})\in \mathcal{A})$ with random variable $\mathsf{s}$, function $f$ and acceptance interval $\mathcal{A}$: 
	
	\bigskip
	
	\begin{enumerate}\setlength\itemsep{.4cm}
		\item Generate $n$ realizations of $\mathsf{s}$
		\item Evaluate $f(s_i)$ for all realizations $s_i$
		\item Count how many $f(s_i)\in \mathcal{A}$ $\Rightarrow$ $n_\mathcal{A}$
		\item $\mathsf{P}(f(\mathsf{s})\in \mathcal{A})\approx \frac{n_\mathcal{A}}{n}$
	\end{enumerate}

	\vspace{.8cm}
	
	$\Rightarrow$ Very generic but high accuracy requires many samples!
	
\end{frame}

\section{WCD Approach}

\begin{frame}
	\frametitle{Worst-Case-Distance (WCD) Approach\footnote[1]{H. E. Graeb, C. U. Wieser, and K. J. Antreich, ''Improved methods for worst-case analysis and optimization incorporating operating tolerances,'' 30th ACM/IEEE Design Automation Conference, pp. 142–147, June 1993.}}
	$\boldsymbol{s}=[s_1,...,s_n]^{\text{T}}\sim \mathcal{N}(\boldsymbol{0},\boldsymbol{C})$,\\
	\vspace{.3cm}acceptance region $\mathcal{A}_{\boldsymbol{s}}$,\\\vspace{.3cm}linear approximation:\\$
%	\begin{equation}
		\mathcal{A}_{\boldsymbol{s}} \approx \{\boldsymbol{s}\in \mathbb{R}^n|\boldsymbol{a}^{\text{T}}\boldsymbol{C}^{-1}\boldsymbol{s}\leq \beta_\text{w}\},$
%	\end{equation}
		
	\vspace{.5cm}
	where $||\boldsymbol{a}||_\beta=1$,\\\vspace{.3cm}$
%	\begin{equation}
		\beta_\text{w}=\min_{\boldsymbol{s}\in \mathbb{R}^n}||\boldsymbol{s}||_\beta\quad\text{s.t.}\quad \boldsymbol{s}\notin \mathcal{A}_{\boldsymbol{s}},$\\\vspace{.3cm}
%	\end{equation}
	and $||\boldsymbol{s}||^2_\beta=\boldsymbol{s}^{\text{T}}\boldsymbol{C}^{-1}\boldsymbol{s}$
	
	\vspace{.6cm}
	
	$
%	\begin{equation}
		\mathsf{P}(\boldsymbol{s}\in \mathcal{A}_{\boldsymbol{s}})\approx\Phi(\beta_\text{w})$\\
%	\end{equation}
	

	\vspace{-5cm}
	
	{
	\hspace{5.5cm}
	\begin{tikzpicture}[scale=.6]
	\draw[->] (-4.5,0)--(4.5,0) node[anchor=west]{$s_1$};
	\draw[->] (0,-4.5)--(0,4.5) node[anchor=west]{$s_2$};
	%		\foreach \x in {-4,...,4}{
	%			\draw (\x,-.1)--(\x,.1)(-.1,\x)--(.1,\x);
	%		}
	\draw[blue, very thick, dashed, font=\Large] (-2,4.5)--(4.5,-.35) (3.5,-3.5)node[]{$\mathcal{A}_{\boldsymbol{s}}$};
	\draw[blue,very thick](-.7,4.5) .. controls (.8,2.3) and (2.5,.7) .. (4.5,.5);
	\fill[blue, opacity=.1](-.7,4.5) .. controls (.8,2.3) and (2.5,.7) .. (4.5,.5)|-(-4.5,-4.5)|-cycle;
	%\fill[blue, opacity=.1] (-2,4.5)--(4.5,-.35)|-(-4.5,-4.5)|-cycle;
	\foreach \x in {1,2,3}
	\draw[red, thick] (0,0) circle[x radius=1.5*\x, y radius=\x] (0,-\x) node[anchor=north east]{$\beta = \x$};
	\draw[very thick, ->] (1,.3) node[]{$\boldsymbol{a}$} (0,0)--(2.26/2,1.33/2);
	\draw[red,thick,|-|](0,0)--(2.26,1.33);
	\draw[red](2.1,.9)node[rotate=30]{$\beta_\text{w}=2$};
	\end{tikzpicture}
	
	}
	
\end{frame}


\begin{frame}
	\frametitle{WCD Approach Applied to Robust Design}
	Measurement errors form the vector $\boldsymbol{s}$\\
	
	\vspace{.3cm}
	Two WCDs $\beta_{\text{w,min}}$ and $\beta_{\text{w,max}}$ corresponding to acceptance regions $\mathcal{A_{\boldsymbol{s},\text{min}}}=\{\boldsymbol{s}|x_\text{end}\geq x_\text{min}\}$ and $\mathcal{A_{\boldsymbol{s},\text{max}}}=\{\boldsymbol{s}|x_\text{end}\leq x_\text{max}\}$\\
	
	\vspace{.8cm}
	\begin{align}\nonumber
		\mathsf{P}(x_\text{min}\leq x_\text{end}\leq x_\text{max})=\mathsf{P}(x_\text{end}\leq x_\text{max})-\mathsf{P}(x_\text{end}\leq x_\text{min})\\
		\approx \Phi(\beta_{\text{w,max}})-\Phi(-\beta_{\text{w,min}})=\hat{\mathsf{P}}(x_\text{min}\leq x_\text{end}\leq x_\text{max})
	\end{align}
	
	\vspace{.5cm}
	
	under the assumption that $x_\text{min}\leq x_\text{end}\leq x_\text{max}$ for $\boldsymbol{s}=\boldsymbol{0}$
\end{frame}

\begin{frame}
	$Q$ vs. $\sigma_x$ and $\tau$ for TTC-based decision rule, $\sigma_v=0$ and $f_\text{s}=100\text{Hz}$
	
			\vspace{.5cm}
			\includegraphics[width=25cm]{plots/actual2d100Hz.pdf}\\
			
			\vspace{-13.7cm}
			
			{\hspace*{6cm}\includegraphics[width=25cm]{plots/wca2d100Hz.pdf}}
		
		\vspace{-10cm}
		
		\begin{center}
			\begin{tabular}{ c | c }
				$x_0$&$10\text{m}$\\\hline
				$v_0$&$-10\frac{\text{m}}{\text{s}}$\\\hline
				$x_\text{min}$&$0\text{m}$\\\hline
				$x_\text{max}$&$0.5\text{m}$\\\hline
				$a_\text{b}$&$10\frac{\text{m}}{\text{s}^2}$
			\end{tabular}
		\end{center}
\end{frame}

\begin{frame}
	Approximation error vs. $\sigma_x$ and $\tau$\\
	\vspace{-.3cm}
	{\hspace*{1.3cm}\includegraphics[width=40cm]{plots/wcaVsActual_error2d100Hz.pdf}}\\\vspace{-15.7cm}
	$\Rightarrow$ straightforward application of WCD approach not well-suited
\end{frame}

%\begin{frame}
%	\frametitle{Illustration of the WCD Approach}
%	\begin{center}
%		\begin{tikzpicture}[scale=.8]
%		\draw[->] (-4.5,0)--(4.5,0) node[anchor=west]{$s_1$};
%		\draw[->] (0,-4.5)--(0,4.5) node[anchor=west]{$s_2$};
%		%		\foreach \x in {-4,...,4}{
%		%			\draw (\x,-.1)--(\x,.1)(-.1,\x)--(.1,\x);
%		%		}
%		\draw[blue, very thick, dashed, font=\Large] (-2,4.5)--(4.5,-.35) (3.5,-3.5)node[]{$\mathcal{A}_{\boldsymbol{s}}$};
%		\draw[blue,very thick](-.7,4.5) .. controls (.8,2.3) and (2.5,.7) .. (4.5,.5);
%		\fill[blue, opacity=.1](-.7,4.5) .. controls (.8,2.3) and (2.5,.7) .. (4.5,.5)|-(-4.5,-4.5)|-cycle;
%		%\fill[blue, opacity=.1] (-2,4.5)--(4.5,-.35)|-(-4.5,-4.5)|-cycle;
%		\foreach \x in {1,2,3}
%		\draw[red, thick] (0,0) circle[x radius=1.5*\x, y radius=\x] (0,-\x) node[anchor=north east]{$\beta = \x$};
%		\draw[very thick, ->] (1,.3) node[]{$\boldsymbol{a}$} (0,0)--(2.26/2,1.33/2);
%		\draw[red,thick,|-|](0,0)--(2.26,1.33);
%		\draw[red](1.9,.8)node[rotate=30]{$\beta_\text{w}=2$};
%		\end{tikzpicture}
%	\end{center}
%\end{frame}


\section{OWCD Approach}

\begin{frame}
	\frametitle{Structure of the Acceptance Region}
	TTC-based triggering, $\sigma_v=0$
	\vspace{.5cm}
	
	Boundary acceptance region $\mathcal{A}_{\boldsymbol{s}}$ consists of many orthogonal hyperplanes:
	
	$\mathcal{B}_i$ : set of errors, for which no intervention is triggered in time step $i$
	
	\vspace{.2cm}
	\begin{center}
		\begin{tabular}{c c}
		\begin{tikzpicture}[scale=0.6]
		\draw[->] (-3.5,0)--(3.5,0) node[anchor=west]{$\varepsilon_{x,3}$};
		\draw[->] (0,-3.5)--(0,3.5) node[anchor=west]{$\varepsilon_{x,4}$};
		
		\draw[blue, dashed] (-3.3,-.4)-|(-1,-3.3);
		\draw[very thick, blue] (3.3,-.4)-|(-1,3.3);
		\fill[green, opacity=.05] (3.3,-.4)-|(-3.3,3.3)-|cycle;
		\fill[red, opacity=.05] (3.3,-3.3)-|(-1,3.3)-|cycle;
		\fill[blue, opacity=.2] (-1,3.3)|-(3.3,-.4)|-cycle;
		\draw[blue, font=\large] (-2.5,1.7)node[]{$\mathcal{B}_{4}$} (1.5,-2.5)node[]{$\mathcal{B}_{3}$} (1.7,1.7)node[]{$\mathcal{B}_{3}\cap \mathcal{B}_{4}\supseteq \mathcal{A}_{\boldsymbol{s}}$};
		\draw[red, thick,|-|] (1,-.4)--(1,0)node[anchor= south]{$\beta_{\text{w,max}}$};
		\end{tikzpicture}  &
		\begin{tikzpicture}[scale=0.6]
		\draw[->] (-3.5,0)--(3.5,0) node[anchor=west]{$\varepsilon_{x,5}$};
		\draw[->] (0,-3.5)--(0,3.5) node[anchor=west]{$\varepsilon_{x,6}$};
		\draw[very thick, blue] (.2,3.3)|-(3.3,.8);
		\draw[blue, dashed] (.2,-3.3)|-(-3.3,.8);
		\fill[blue, opacity=.2](-3.3,3.3)-|(.2,.8)-|(3.3,-3.3)-|cycle;
		\fill[green,opacity=.05](-3.3,3.3)|-(3.3,.8)|-cycle;
		\fill[red,opacity=.05](.2,3.3)|-(3.3,-3.3)|-cycle;
		\draw[blue, font=\large] (-2,2.2)node[]{$\mathcal{B}_{6}$} (1.7,-2) node[]{$\mathcal{B}_5$} (-2.2,-1.7)node[]{$(\mathcal{B}_5\cap \mathcal{B}_6)^\text{C} \supseteq \mathcal{A}_{\boldsymbol{s}}$};
		\draw[red, thick,|-|] (.2,.8)--(0,0)node[anchor= south east]{$\beta_{\text{w,min}}$};
		\draw[blue,thick](1.7,1.7)node[]{$\mathcal{B}_{5}\cap \mathcal{B}_{6}$};
		\end{tikzpicture}\\
		must not trigger&must trigger at least once
	\end{tabular}
	\end{center}
\end{frame}

%\begin{frame}
%	\frametitle{OWCD Approach - Boundary Visualization (1)}
%	\begin{center}
%		\begin{tikzpicture}
%		\draw[->] (-3.5,0)--(3.5,0) node[anchor=west]{$\varepsilon_{x,3}$};
%		\draw[->] (0,-3.5)--(0,3.5) node[anchor=west]{$\varepsilon_{x,4}$};
%		
%		\draw[blue, dashed] (-3.3,-.4)-|(-1,-3.3);
%		\draw[very thick, blue] (3.3,-.4)-|(-1,3.3);
%		\fill[green, opacity=.05] (3.3,-.4)-|(-3.3,3.3)-|cycle;
%		\fill[red, opacity=.05] (3.3,-3.3)-|(-1,3.3)-|cycle;
%		\fill[blue, opacity=.2] (-1,3.3)|-(3.3,-.4)|-cycle;
%		\draw[blue, font=\large] (-2.5,1.7)node[]{$\mathcal{B}_{4}$} (1.5,-2.5)node[]{$\mathcal{B}_{3}$} (1.7,1.7)node[]{$\mathcal{B}_{3}\cap \mathcal{B}_{4}\supseteq \mathcal{A}_{\boldsymbol{s}}$};
%		\draw[red, thick,|-|] (1,-.4)--(1,0)node[anchor= south]{$\beta_{\text{w,max}}$};
%		\end{tikzpicture}
%	\end{center}
%\end{frame}
%
%\begin{frame}
%	\frametitle{OWCD Approach - Boundary Visualization (2)}
%	\begin{center}
%		\begin{tikzpicture}
%		\draw[->] (-3.5,0)--(3.5,0) node[anchor=west]{$\varepsilon_{x,5}$};
%		\draw[->] (0,-3.5)--(0,3.5) node[anchor=west]{$\varepsilon_{x,6}$};
%		\draw[very thick, blue] (.2,3.3)|-(3.3,.8);
%		\draw[blue, dashed] (.2,-3.3)|-(-3.3,.8);
%		\fill[blue, opacity=.2](-3.3,3.3)-|(.2,.8)-|(3.3,-3.3)-|cycle;
%		\fill[green,opacity=.05](-3.3,3.3)|-(3.3,.8)|-cycle;
%		\fill[red,opacity=.05](.2,3.3)|-(3.3,-3.3)|-cycle;
%		\draw[blue, font=\large] (-2,2.2)node[]{$\mathcal{B}_{6}$} (1.7,-2) node[]{$\mathcal{B}_5$} (-1.7,-1.7)node[]{$(\mathcal{B}_5\cap \mathcal{B}_6)^\text{C} \supseteq \mathcal{A}_{\boldsymbol{s}}$};
%		\draw[red, thick,|-|] (.2,.8)--(0,0)node[anchor= south east]{$\beta_{\text{w,min}}$};
%		\draw[blue,thick](1.7,1.7)node[]{$\mathcal{B}_{5}\cap \mathcal{B}_{6}$};
%		\end{tikzpicture}
%	\end{center}
%\end{frame}

\begin{frame}
	\frametitle{Orthogonal WCD (OWCD) Approach (1)}
	
%	\vspace{.5cm}
	
	\begin{block}
		{Basic Idea}
		Use the WCD approach in each time step and combine results properly exploiting the orthogonality of the hyperplanes
	\end{block}

	\vspace{0cm}
	
	\begin{center}
		\begin{tabular}{c c}
			\begin{tikzpicture}[scale=0.6]
			\draw[->] (-3.5,0)--(3.5,0) node[anchor=west]{$\varepsilon_{x,3}$};
			\draw[->] (0,-3.5)--(0,3.5) node[anchor=west]{$\varepsilon_{x,4}$};
			
			\draw[blue, dashed] (-3.3,-.4)-|(-1,-3.3);
			\draw[very thick, blue] (3.3,-.4)-|(-1,3.3);
			\fill[green, opacity=.05] (3.3,-.4)-|(-3.3,3.3)-|cycle;
			\fill[red, opacity=.05] (3.3,-3.3)-|(-1,3.3)-|cycle;
			\fill[blue, opacity=.2] (-1,3.3)|-(3.3,-.4)|-cycle;
			\draw[blue, font=\large] (-2.5,1.7)node[]{$\mathcal{B}_{4}$} (1.5,-2.5)node[]{$\mathcal{B}_{3}$} (1.7,1.7)node[]{$\mathcal{B}_{3}\cap \mathcal{B}_{4}\supseteq \mathcal{A}_{\boldsymbol{s}}$};
			\draw[red, thick,|-|] (0,.5)--(-1,.5)node[anchor=east]{$\beta_{\text{w,3}}$};
			\draw[red, thick,|-|] (.5,0)--(.5,-.40)node[anchor=north]{$\beta_{\text{w,4}}$};
			\end{tikzpicture}  &
			\begin{tikzpicture}[scale=0.6]
			\draw[->] (-3.5,0)--(3.5,0) node[anchor=west]{$\varepsilon_{x,5}$};
			\draw[->] (0,-3.5)--(0,3.5) node[anchor=west]{$\varepsilon_{x,6}$};
			\draw[very thick, blue] (.2,3.3)|-(3.3,.8);
			\draw[blue, dashed] (.2,-3.3)|-(-3.3,.8);
			\fill[blue, opacity=.2](-3.3,3.3)-|(.2,.8)-|(3.3,-3.3)-|cycle;
			\fill[green,opacity=.05](-3.3,3.3)|-(3.3,.8)|-cycle;
			\fill[red,opacity=.05](.2,3.3)|-(3.3,-3.3)|-cycle;
			\draw[blue, font=\large] (-2,2.2)node[]{$\mathcal{B}_{6}$} (1.7,-2) node[]{$\mathcal{B}_5$} (-2.5,-1.7)node[]{$(\mathcal{B}_5\cap \mathcal{B}_6)^\text{C} \supseteq \mathcal{A}_{\boldsymbol{s}}$};
			\draw[red, thick,|-|] (.2,1.2)--(0,1.2)node[anchor=east]{$\beta_{\text{w,5}}$};
			\draw[red, thick,|-|] (.7,.8)--(.7,0)node[anchor=north]{$\beta_{\text{w,6}}$};
			\end{tikzpicture}
		\end{tabular}
	\end{center}
	

	
%	\begin{equation}
%		\mathsf{P}(x_\text{min}\leq x_\text{end}\leq x_\text{max}) = \left(\prod_{i\in \mathcal{I}_{\text{bad}}}\mathsf{P}(\overline{T}_i)\right)\left(1-\prod_{i\in \mathcal{I}_{\text{good}}}\mathsf{P}(\overline{T}_i)\right)
%	\end{equation}
	
%	\vspace{-.2cm}
	
	\begin{equation}
		\beta_{\text{w},i}=\min_{\varepsilon_{x,i},\varepsilon_{v,i}}||[\varepsilon_{x,i},\varepsilon_{v,i}]^{\text{T}}||_\beta\quad \text{s.t.}\quad\hat{x}_i=-\tau \hat{v}_i
	\end{equation}
	
	
	
\end{frame}

\begin{frame}
	\frametitle{Orthogonal WCD (OWCD) Approach (2)}
	Probability of not triggering an intervention in time step $i$
	\begin{equation}\label{eq:pNotTrig}
	\mathsf{P}(\overline{T}_i)=\begin{cases}
	\Phi(\beta_{\text{w},i}),&\boldsymbol{0}\in \mathcal{B}_i\\
	1-\Phi(\beta_{\text{w},i}),&\text{else}
	\end{cases}
	\end{equation}
	
	\begin{equation}
		\mathsf{P}(x_\text{min}\leq x_\text{end}\leq x_\text{max}) = \left(\prod_{i\in \mathcal{I}_{\text{bad}}}\mathsf{P}(\overline{T}_i)\right)\left(1-\prod_{i\in \mathcal{I}_{\text{good}}}\mathsf{P}(\overline{T}_i)\right)
	\end{equation}
	
	\vspace{.6cm}
	$\mathcal{I}_\text{bad}$ : time steps in which no intervention must be triggered in order to fulfill $x_\text{min}\leq x_\text{end}\leq x_\text{max}$\\
	\vspace*{.3cm}
	$\mathcal{I}_\text{good}$ : time steps in which an intervention must be triggered at least once in order to fulfill $x_\text{min}\leq x_\text{end}\leq x_\text{max}$\\
	\vspace{.6cm}
	Index sets can be determined by a few simulations of the system and is only required once
\end{frame}

\begin{frame}
	\frametitle{OWCD Approach - Results for TTC-Based Decision Rule}
	$\sigma_v=0$ and $f_\text{s}=100\text{Hz}$
	\vspace{.1cm}
	
	Decision boundary is linear $\Rightarrow$ OWCD approach gives exact results!
	
	\vspace{-.2cm}
		
			\includegraphics[width=25cm]{plots/actual2d100Hz.pdf}\\
	
	
			\vspace{-13.67cm}\hspace*{6cm}\includegraphics[width=25cm]{plots/owca2d100Hz.pdf}
	\vspace{-10cm}
	
	Example: $\tau=0.521\text{s}$, $\sigma_x=0.143\text{m}$ yields $\hat{\mathsf{P}}(x_\text{min}\leq x_\text{end}\leq x_\text{max})=0.992$
	\begin{itemize}
		\item Determining index sets requires $100$ system simulations
		\item Evaluating WCDs requires $4508$ evaluations of decision rule
		\item MC: $5\cdot10^6$ simulations for confidence interval $[0.992-10^{-4},0.992+10^{-4}]$ at confidence level $99\%$\footnote[2]{H. E. Graeb, Analog Design Centering and Sizing. Springer Netherlands,
			2007.}
	\end{itemize}
	

\end{frame}

\begin{frame}
	\frametitle{OWCD Approach - Results for BTN-Based Decision Rule}
	$f_\text{s}=50\text{Hz}$
	
	\vspace{.1cm}
	
	Despite non-linear boundary OWCD approach still gives good results
	
	\vspace{.1cm}
	
%	\begin{itemize}\setlength\itemsep{.3cm}
%		\item $1263$ function and constraint evaluations using \texttt{fmincon()} 
%		\item Error at $\mathsf{P}=0.9972$: $-3.5\cdot10^{-5}$
%		\item Monte Carlo would need $2\cdot10^{6}$\\evaluations for similar results
%	\end{itemize}
	
	
	\vspace{-.0cm}\hspace*{0cm}\includegraphics[width=5cm]{plots/omc1e6BTN3d50Hz.pdf}
	
	\vspace{-2.5cm}\hspace*{5.5cm}\includegraphics[width=5cm]{plots/owcaBTN3d50Hz.pdf}
	
	{\tiny\hspace{.7cm}{Contour surfaces of $\mathsf{P}(x_\text{min}\leq x_\text{end}\leq x_\text{max})$}\hspace{1.8cm}{Contour surfaces of $\hat{\mathsf{P}}(x_\text{min}\leq x_\text{end}\leq x_\text{max})$}}
	
	
	\vspace{.15cm}
	Example: $\tau=9.31\frac{\text{m}}{\text{s}^2}$, $\sigma_x=0.0724\text{m}$, $\sigma_v=0.0724\frac{\text{m}}{\text{s}}$ yields $\hat{\mathsf{P}}(x_\text{min}\leq x_\text{end}\leq x_\text{max})=0.99722$ and error $\hat{\mathsf{P}}-\mathsf{P}=-3.5\cdot 10^{-5}$
	\begin{itemize}
		\item Determining index sets requires $50$ system simulations
		\item Evaluating WCDs requires $1263$ evaluations of decision rule
		\item MC: $2\cdot10^6$ simulations for confidence interval $[0.99725-10^{-4},0.99725+10^{-4}]$ at confidence level $99\%$\footnote[3]{H. E. Graeb, Analog Design Centering and Sizing. Springer Netherlands,
			2007.}
	\end{itemize}
	
\end{frame}

\section{Conclusion}

\begin{frame}
	\frametitle{Conclusion}
	\vspace{.2cm}

		OWCD approach can reduce computational effort in simulation based robust vehicular safety system design by orders of magnitude
		\vspace{.1cm}
		\begin{itemize}
			\item based on WCD approach from integrated circuit design
			\item approximates probability of fulfilling a performance specification
			\item replaces expensive MC simulation by a few relevant system simulations
		\end{itemize}
		\vspace{.3cm}
		Non-linearities limit performance of OWCD approach
		
		\vspace{.5cm}
		
		Future work:
		\begin{itemize}
			\item Consider further uncertainties (e.g., random braking deceleration)
			\item Examine effects of stochastic dependency between measurement errors
		\end{itemize}

\end{frame}

%\begin{frame}
%	\bibliographystyle{IEEEtran}
%	\bibliography{ThesisPresentation}
%\end{frame}


\end{document}